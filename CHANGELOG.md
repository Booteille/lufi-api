# Changelog

## Unreleased
### Added

- Added SLICE_STARTED event
- Add waitForStart() method
- Expose JobStatus

### Changed

- Remove main worker to improve performances
- Use workers for archive operations
- Improve memory usage
- Use ArrayBuffer with sjcl encryption
- Transfer chunk to the encrypt worker instead of copying it

### Fixed

- Fix upload of 0kb files
- Fix name of files without extensions
- Fix socket url building

## [v0.2.0] - 2024-11-13

### Added

- Refactor to use Web Workers
- Add file upload
- Add file download
- Add file zipping
- Add file deletion
- Add upload cancellation
- Add function to retrieve upload informations
- Add function to fetch server config
- Add better tests
- Add better error handling using neverthrow
- Add events allowing to get informations about different states (such as downloading/uploading file state)
- Add queue system for uploading/downloading
- Add pause/resume mecanism
- Automatically use Sjcl for uploads if context is not secured
- Password hashing client-side if Lufi server version is before 0.08.0

### Changed

- Update dependencies
- Now use Deno instead of Bun
- Do not encode/decode to base64 before/after encryption/decryption
- Prettier now adds semilicons
- Slice length is now exported
- Remove "wait" function in preference of an event system

### Fixed

- Fix Sjcl encryption/decryption
- Fix algorithm detection in decrypt()
- Fix benchmark

## [v0.1.1] - 2024-06-28

### Added

- Add npm package configuration

### Changed

- Update dependencies
- Update README
- Remove unused dependency

## [v0.1.0] - 2024-06-26

### Added

- Initial Commit

[v0.2.0]: https://codeberg.org/Booteille/lufi-api/compare/v0.2.0..v0.1.1
[v0.1.1]: https://codeberg.org/Booteille/lufi-api/compare/v0.1.1..v0.1.0
[v0.1.0]: https://codeberg.org/Booteille/lufi-api/releases/tag/v0.1.0
