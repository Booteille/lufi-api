import { CryptoAlgorithm } from "~/enum/crypto-algorithm.ts";
import type { ResultAsync } from "neverthrow";
import type { CryptoError } from "~/error/crypto/crypto-error.ts";
import type { DecryptionError } from "~/error/crypto/decryption-error.ts";
import type { EncryptionError } from "~/error/crypto/encryption-error.ts";
import type { EncryptedData } from "~/interface/encrypted-data.ts";
import * as sjcl from "~/api/crypto/sjcl.ts";
import * as web from "~/api/crypto/web.ts";
import type { HashingError } from "~/error/crypto/hashing-error.ts";

/**
 * Decrypt an EncryptedData object using the key used for encryption
 *
 * @param key
 * @param value
 * @returns
 */
export const decrypt = (
  key: string,
  value: EncryptedData,
): ResultAsync<ArrayBuffer, DecryptionError> =>
  value.algo === undefined || value.algo === CryptoAlgorithm.Sjcl
    ? sjcl.decrypt(key, value)
    : web.decrypt(key, value);

/**
 * Encrypt an ArrayBuffer using the provided key and algorithm
 *
 * @param key
 * @param value
 * @param algo
 * @returns
 */
export const encrypt = (
  key: string,
  value: ArrayBuffer,
  algo: CryptoAlgorithm,
): ResultAsync<EncryptedData, EncryptionError> =>
  (algo === CryptoAlgorithm.Sjcl)
    ? sjcl.encrypt(key, value)
    : web.encrypt(key, value);

/**
 * Generate a new key for encryption/decryption
 *
 * @param algo
 * @returns
 */
export const generateKey = (
  algo = CryptoAlgorithm.WebCrypto,
): ResultAsync<string, CryptoError> =>
  algo === CryptoAlgorithm.Sjcl ? sjcl.generateKey() : web.generateKey();

/**
 * Hash a password using the provided algorithm
 *
 * @param password
 * @param algo
 * @returns
 */
export const hashPassword = (
  password: string,
  algo: CryptoAlgorithm,
): ResultAsync<string, HashingError> =>
  algo === CryptoAlgorithm.Sjcl
    ? sjcl.hashPassword(password)
    : web.hashPassword(password);
