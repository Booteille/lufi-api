import {
  Decode as b64urldecode,
  Encode as b64urlencode,
} from "arraybuffer-encoding/base64/url";
import type { EncryptedData } from "~/interface/encrypted-data.ts";
import { CryptoAlgorithm } from "~/enum/crypto-algorithm.ts";
import { DecryptionError } from "~/error/crypto/decryption-error.ts";
import { okAsync, ResultAsync } from "neverthrow";
import { ensureError } from "~/utils.ts";
import { EncryptionError } from "~/error/crypto/encryption-error.ts";
import { CryptoError } from "~/error/crypto/crypto-error.ts";
import { HashingError } from "~/error/crypto/hashing-error.ts";

/**
 * Decrypt an encryptedData using the key used for encryption
 *
 * @param key
 * @param encrypted
 * @returns
 */
export const decrypt = (
  key: string,
  encrypted: EncryptedData,
): ResultAsync<ArrayBuffer, DecryptionError> => {
  return importKey(key).andThen((importedKey) =>
    ResultAsync.fromPromise(
      crypto.subtle.decrypt(
        {
          name: "AES-GCM",
          iv: encrypted.iv as Uint8Array,
        },
        importedKey,
        encrypted.data as ArrayBuffer,
      ),
      (error) => new DecryptionError(undefined, { cause: ensureError(error) }),
    )
  );
};

/**
 * Encrypt an ArrayBuffer into an EncryptedData using the provided key
 * @param key
 * @param value
 * @returns
 */
export const encrypt = (
  key: string,
  value: ArrayBuffer,
): ResultAsync<EncryptedData, EncryptionError> => {
  return importKey(key).andThen((importedKey) => {
    const iv = crypto.getRandomValues(new Uint8Array(12));
    return ResultAsync.fromPromise(
      crypto.subtle.encrypt(
        {
          name: "AES-GCM",
          iv,
        },
        importedKey,
        value,
      ),
      (error) =>
        new EncryptionError(undefined, {
          cause: ensureError(error),
        }),
    ).andThen((encrypted) => {
      return okAsync({
        algo: CryptoAlgorithm.WebCrypto,
        data: encrypted,
        iv,
      });
    });
  });
};

/**
 * Transform a string into a CryptoKey, usable in Web Crypto API
 *
 * @param key
 * @returns
 */
export const importKey = (key: string): ResultAsync<CryptoKey, CryptoError> => {
  return ResultAsync.fromPromise(
    crypto.subtle.importKey(
      "raw",
      b64urldecode(key),
      { name: "AES-GCM" },
      false,
      [
        "encrypt",
        "decrypt",
      ],
    ),
    (error) =>
      new CryptoError("Unable to import cryptography key", {
        cause: ensureError(error),
      }),
  );
};

/**
 * Generate a random string using Web Crypto API.
 *
 * @returns
 */
export const generateKey = (): ResultAsync<string, CryptoError> => {
  return ResultAsync.fromPromise(
    new Promise((resolve, reject) =>
      crypto.subtle
        .generateKey(
          {
            name: "AES-GCM",
            length: 256,
          },
          true,
          ["encrypt", "decrypt"],
        )
        .then((generatedKey) =>
          crypto.subtle
            .exportKey("raw", generatedKey)
            .then((key) => resolve(b64urlencode(key)))
            .catch((error) => {
              reject(
                new CryptoError("Unable to base64 encode the url", {
                  cause: ensureError(error),
                }),
              );
            })
        )
        .catch((error) => reject(error))
    ),
    (error) =>
      new CryptoError("Unable to generate key", { cause: ensureError(error) }),
  );
};

/**
 * Hash a password using WebCrypto API
 *
 * @param password
 * @returns
 */
export const hashPassword = (
  password: string,
): ResultAsync<string, HashingError> => {
  const promise = async () => {
    return Array.from(
      new Uint8Array(
        await crypto.subtle.digest(
          "SHA-512",
          new TextEncoder().encode(password),
        ),
      ),
    ).map((b) => b.toString(16).padStart(2, "0")).join("");
  };

  return ResultAsync.fromPromise(
    promise(),
    (error) => new HashingError(undefined, { cause: ensureError(error) }),
  );
};
