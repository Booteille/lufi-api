import { errAsync, okAsync, ResultAsync } from "neverthrow";
import type { LufiFile } from "~/entities/lufi-file.ts";
import { EVENT } from "~/enum/event.ts";
import { UPLOAD_STATUS } from "~/enum/file-status.ts";
import { SocketPath } from "~/enum/socket-path.ts";
import type { UploadError } from "~/error/upload/upload-error.ts";
import { WebSocketConnectionError } from "~/error/websocket/websocket-connection-error.ts";
import { WebSocketError } from "~/error/websocket/websocket-error.ts";
import type { ClientUploadChunkMetadata } from "~/interface/client-upload-chunk-metadata.ts";
import type { EncryptedData } from "~/interface/encrypted-data.ts";
import type { ServerCancelMetadata } from "~/interface/server-cancel-metadata.ts";
import type { ServerDownloadChunkSuccessMetadata } from "~/interface/server-download-chunk-success-metadata.ts";
import type { ServerUploadChunkMetadata } from "~/interface/server-upload-chunk-metadata.ts";
import type { ServerDownloadChunkMetadata } from "~/type/server-download-chunk-metadata.ts";
import { ensureError } from "~/utils.ts";
import { events, updateFile } from "~/worker/shared.ts";
import * as crypto from "~/api/crypto.ts";
import {
  Decode as b64decode,
  Encode as b64encode,
} from "arraybuffer-encoding/base64";

export const sockets: {
  [key: string]: WebSocket;
} = {};

const MAX_ERRORS = 5;

/**
 * Handle WebSocket response for cancel request
 *
 * @param data
 * @returns
 */
const onCancelMessage = (
  data: ServerCancelMetadata,
): ResultAsync<void, Error> => {
  events.emit(EVENT.UPLOAD_CANCELLED, data.success);

  return okAsync(undefined);
};

/**
 * Handle WebSocket response for download request
 *
 * @param response
 * @param lufiFile
 * @returns
 */
const onDownloadMessage = (
  response: string,
  lufiFile: LufiFile,
): ResultAsync<void, WebSocketError> => {
  const result = response.split("XXMOJOXX");
  const metadataString = result.shift();

  if (metadataString !== undefined) {
    const metadata = JSON.parse(metadataString) as ServerDownloadChunkMetadata;

    if (isServerDownloadChunkSuccessMetadata(metadata)) {
      const dataString = result.shift();

      if (dataString) {
        const encryptedData: EncryptedData = JSON.parse(dataString);

        // If file was uploaded using Lufi API
        if (encryptedData.iv) {
          encryptedData.iv = new Uint8Array(Object.values(encryptedData.iv));
          encryptedData.data = b64decode(encryptedData.data as string);
        }

        return crypto.decrypt(lufiFile.keys.client, encryptedData).andThen(
          (decryptedPart) => {
            const buffer = typeof decryptedPart === "string"
              ? (new TextEncoder().encode(decryptedPart).buffer as ArrayBuffer)
              : decryptedPart;

            // If first chunk
            if (metadata.part === 0) {
              updateFile(lufiFile, {
                chunksReady: lufiFile.chunksReady + 1,
                delAtFirstView: metadata.del_at_first_view,
                delay: metadata.delay,
                name: metadata.name,
                size: metadata.size,
                totalChunks: metadata.total,
                type: metadata.type,
                zipped: metadata.zipped,
              });

              events.emit(EVENT.DOWNLOAD_STARTED);
            } else {
              updateFile(lufiFile, { chunksReady: lufiFile.chunksReady + 1 });
            }

            events.emit(EVENT.CHUNK_DOWNLOADED, buffer, metadata.part);

            if (lufiFile.chunksReady === metadata.total) {
              return endDownload(lufiFile).andThen(() => {
                events.emit(EVENT.DOWNLOAD_COMPLETE);
                events.emit(EVENT.SOCKET_OPERATION_TERMINATED);

                return okAsync(undefined);
              });
            }

            return okAsync(undefined);
          },
        );
      } else {
        const error = new WebSocketError(
          "Cannot retrieve metadata from data received by the server",
        );

        events.emit(EVENT.OPERATION_FAILED, error);
        return errAsync(error);
      }
    } else {
      const error = new WebSocketError(metadata.msg);

      events.emit(EVENT.OPERATION_FAILED, error);
      return errAsync(error);
    }
  } else {
    const error = new WebSocketError(
      "Cannot retrieve metadata from data received by the server",
    );

    events.emit(EVENT.OPERATION_FAILED, error);
    return errAsync(error);
  }
};

/**
 * Handle WebSocket response for upload request
 *
 * @param response
 * @param lufiFile
 * @returns
 */
const onUploadMessage = (
  response: ServerUploadChunkMetadata,
  lufiFile: LufiFile,
): ResultAsync<void, UploadError> => {
  if (response.success) {
    // If first chunk
    if (response.j === 0) {
      // console.info(`Upload of ${lufiFile.keys.client} started`);

      updateFile(lufiFile, {
        keys: { client: lufiFile.keys.client, server: response.short },
        actionToken: response.token,
        queueIndex: response.i,
      });

      events.emit(EVENT.UPLOAD_STARTED);
    }

    updateFile(lufiFile, {
      chunksReady: lufiFile.chunksReady + 1,
      createdAt: response.created_at,
    });

    events.emit(EVENT.CHUNK_UPLOADED);

    if (lufiFile.chunksReady === lufiFile.totalChunks) {
      updateFile(lufiFile, { uploadStatus: UPLOAD_STATUS.COMPLETE });

      events.emit(EVENT.UPLOAD_COMPLETE);
      events.emit(EVENT.SOCKET_OPERATION_TERMINATED);
    }

    return okAsync(undefined);
  } else {
    const error = new WebSocketError(response.msg);
    events.emit(EVENT.OPERATION_FAILED, error);

    return errAsync(error);
  }
};

/**
 * Called on sockets "onmessage" event
 *
 * @param e
 * @param socketUrl
 * @returns
 */
const onMessage = (
  e: MessageEvent,
  lufiFile: LufiFile,
): ResultAsync<void, UploadError> => {
  const data = tryParseJson(e.data);

  let callback;

  if (data) {
    if (!data.action && data.msg) {
      // If error
      const error = new WebSocketError(data.msg);
      events.emit(EVENT.OPERATION_FAILED, error);

      return errAsync(error);
    } else {
      if ("delay" in data) {
        callback = onUploadMessage(data, lufiFile);
      } else {
        callback = onCancelMessage(data);
      }
    }
  } else {
    callback = onDownloadMessage(e.data, lufiFile);
  }

  return callback;
};

/**
 * Is socket connecting?
 *
 * @param socketKey
 * @returns
 */
export const isConnecting = (socketKey: string): boolean =>
  sockets !== undefined &&
  sockets[socketKey] !== undefined &&
  sockets[socketKey].readyState === WebSocket.CONNECTING;

/**
 * Is socket spawned?
 *
 * @param socketKey
 * @returns
 */
export const isSpawned = (socketKey: string): boolean =>
  sockets !== undefined &&
  sockets[socketKey] !== undefined &&
  sockets[socketKey].readyState === WebSocket.OPEN;

/**
 * Ask WebSocket to cancel an upload
 *
 * @param lufiFile
 * @returns
 */
export const cancelUpload = (
  lufiFile: LufiFile,
): ResultAsync<void, WebSocketError> => {
  return sendMessage(
    uploadSocketUrl(lufiFile),
    lufiFile,
    `${
      JSON.stringify({
        id: lufiFile.keys.server,
        mod_token: lufiFile.actionToken,
        cancel: true,
        i: lufiFile.queueIndex,
      })
    }XXMOJOXXuseless`,
  );
};

/**
 * Download a part of the file through the WebSocket
 *
 * @param lufiFile
 * @param chunkNumber
 * @returns
 */
export const downloadChunk = (
  lufiFile: LufiFile,
  chunkNumber: number,
): ResultAsync<void, WebSocketError> => {
  let message;

  if (lufiFile.password) {
    message = { part: chunkNumber, file_pwd: lufiFile.password };
  } else {
    message = { part: chunkNumber };
  }

  return sendMessage(
    downloadSocketUrl(lufiFile),
    lufiFile,
    JSON.stringify(message),
  );
};

/**
 * Tell the WebSocket the download ended
 *
 * @param lufiFile
 * @returns
 */
export const endDownload = (
  lufiFile: LufiFile,
): ResultAsync<void, WebSocketError> => {
  let message: { ended: true; file_pwd?: string };

  if (lufiFile.password) {
    message = { ended: true, file_pwd: lufiFile.password };
  } else {
    message = { ended: true };
  }

  return sendMessage(
    downloadSocketUrl(lufiFile),
    lufiFile,
    JSON.stringify(message),
  );
};

/**
 * Upload a chunk of the file through the WebSocket
 *
 * @param lufiFile
 * @param metadata
 * @param encryptedData
 * @returns
 */
export const uploadChunk = (
  lufiFile: LufiFile,
  metadata: ClientUploadChunkMetadata,
  encryptedData: EncryptedData,
): ResultAsync<void, WebSocketError> => {
  encryptedData.data = b64encode(encryptedData.data as ArrayBuffer);

  return sendMessage(
    uploadSocketUrl(lufiFile),
    lufiFile,
    `${JSON.stringify(metadata)}XXMOJOXX${JSON.stringify(encryptedData)}`,
  );
};

/**
 * Send a message to the WebSocket
 *
 * @param socketUrl
 * @param message
 * @param hasPriority
 * @returns
 */
const sendMessage = (
  socketUrl: string,
  lufiFile: LufiFile,
  message: string,
): ResultAsync<void, WebSocketError> => {
  if (!isSpawned(socketUrl)) {
    return spawn(socketUrl).andThen(() => {
      sockets[socketUrl].onmessage = (e) => onMessage(e, lufiFile);

      return sendMessage(socketUrl, lufiFile, message);
    });
  } else {
    sockets[socketUrl].send(message);

    return okAsync(undefined);
  }
};

const initSocket = (
  socketKey: string,
  errorCount: number = 0,
): Promise<string> => {
  return new Promise((resolve, reject) => {
    sockets[socketKey] = new WebSocket(socketKey);

    events.once(EVENT.SOCKET_OPERATION_TERMINATED, () => {
      sockets[socketKey].close();
    });

    events.once(EVENT.OPERATION_FAILED, () => {
      events.emit(EVENT.SOCKET_OPERATION_TERMINATED);
    });

    sockets[socketKey].onopen = () => {
      // console.info(`Websocket ${socketKey} has been open`);
      events.emit(EVENT.SOCKET_OPENED);
      resolve(socketKey);
    };

    sockets[socketKey].onclose = () => {
      // console.info(`Websocket ${socketKey} has been closed`);
      resolve(socketKey);
    };

    sockets[socketKey].onerror = (event: Event) => {
      if (++errorCount <= MAX_ERRORS) {
        console.error(
          `An error happened while trying to connect to WebSocket "${socketKey}". Trying again. ${errorCount} / ${MAX_ERRORS}`,
          (event as ErrorEvent).error,
        );

        initSocket(socketKey, errorCount);
      } else {
        events.emit(EVENT.SOCKET_ONERROR);
        reject(new WebSocketConnectionError());
      }
    };
  });
};

/**
 * Spawn a new WebSocket or reuse an existing one.
 *
 * @param socketKey
 * @param errorCount
 * @returns
 */
export const spawn = (
  socketKey: string,
): ResultAsync<string, WebSocketError> => {
  if (!isSpawned(socketKey)) {
    if (!isConnecting(socketKey)) {
      // console.info(`Spawning WebSocket ${socketUrl}`);
      return ResultAsync.fromPromise(
        initSocket(socketKey),
        (error) => ensureError(error),
      );
    } else {
      return waitForConnection(socketKey)
        .andThen(() => okAsync(socketKey))
        .orElse((error) => errAsync(error));
    }
  } else {
    return okAsync(socketKey);
  }
};

/**
 * Wait for WebSocket to open. Returns an error if too many connection attempts are made.
 *
 * @param socketKey
 * @returns
 */
export const waitForConnection = (
  socketKey: string,
): ResultAsync<void, WebSocketError> =>
  ResultAsync.fromPromise(
    new Promise((resolve, reject) => {
      if (!isSpawned(socketKey)) {
        events.once(EVENT.SOCKET_OPENED, () => {
          resolve(undefined);
        });

        events.once(EVENT.SOCKET_ONERROR, () => {
          reject(new WebSocketConnectionError());
        });
      } else {
        resolve(undefined);
      }
    }),
    (error) => ensureError(error),
  );

/**
 * Close the WebSocket
 * @param socketKey
 * @returns
 */
export const close = (socketKey: string): ResultAsync<string, WebSocketError> =>
  ResultAsync.fromPromise(
    new Promise((resolve, reject) => {
      if (isSpawned(socketKey)) {
        const timeoutID = setTimeout(() => {
          reject(new WebSocketError("Unable to close the WebSocket"));
        }, 1000);

        sockets[socketKey].onclose = () => {
          clearTimeout(timeoutID);
          resolve(socketKey);
        };

        sockets[socketKey].close();
      } else {
        resolve(socketKey);
      }
    }),
    (error) => ensureError(error),
  );

/**
 * Transforms an instance URL in a WebSocket URL
 *
 * @param instanceUrl
 * @param pathname
 * @returns
 */
export const buildSocketUrl = (instanceUrl: URL, pathname: string): URL => {
  const url = new URL(instanceUrl);

  if (!["ws:", "wss:"].includes(url.protocol)) {
    url.protocol = url.protocol === "http:" ? "ws:" : "wss:";
  }
  url.pathname += pathname;

  return new URL(url.origin + url.pathname);
};

/**
 * Generate the download URL for the socket. Returns a string since it's mostly used as sockets key
 *
 * @param lufiFile
 * @returns
 */
export const downloadSocketUrl = (lufiFile: LufiFile): string => {
  return buildSocketUrl(
    new URL(lufiFile.serverUrl),
    SocketPath.DOWNLOAD + `/${lufiFile.keys.server}`,
  ).toString();
};

/**
 * Generate the upload URL for the socket. Returns a string since it's mostly used as sockets key
 *
 * @param lufiFile
 * @returns
 */
export const uploadSocketUrl = (lufiFile: LufiFile): string => {
  return buildSocketUrl(new URL(lufiFile.serverUrl), SocketPath.UPLOAD)
    .toString();
};

/**
 * Try to parse a string into a JSON. Returns false if not possible.
 *
 * @param data
 * @returns
 */
const tryParseJson = (data: string) => {
  try {
    const parsedObject = JSON.parse(data);

    if (parsedObject && typeof parsedObject === "object") {
      return parsedObject;
    }
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
  } catch (_e) {
    /* empty */
  }
  return false;
};

/**
 * Check if the type of the message received from the server is ServerDownloadChunkMetadata
 *
 * @param message
 * @returns
 */
export const isServerDownloadChunkSuccessMetadata = (
  message: ServerDownloadChunkMetadata,
): message is ServerDownloadChunkSuccessMetadata =>
  typeof message === "object" && message !== null && !("msg" in message);
