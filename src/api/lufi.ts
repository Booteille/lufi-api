import EventEmitter from "events";
import { errAsync, okAsync, ResultAsync } from "neverthrow";
import { LufiFile } from "~/entities/lufi-file.ts";
import { LufiJob } from "~/entities/lufi-job.ts";
import { JobStatus } from "~/enum/job-status.ts";
import { EVENT } from "~/enum/event.ts";
import { UPLOAD_STATUS } from "~/enum/file-status.ts";
import { WORKER_ACTION } from "~/enum/worker-action.ts";
import { ensureError, fetchServerConfig, isSecureContext } from "~/utils.ts";
import * as crypto from "~/api/crypto.ts";
import type { FileSlicingError } from "~/error/file/file-slicing-error.ts";
import { CryptoAlgorithm } from "~/enum/crypto-algorithm.ts";
import type { CryptoError } from "~/error/crypto/crypto-error.ts";
import { DownloadError } from "~/error/download/download-error.ts";
import { InfosError } from "~/error/infos-error.ts";
import { JobPauseError } from "../error/job/job-pause-error.ts";
import { JobResumeError } from "~/error/job/job-resume-error.ts";
import { JobError } from "~/error/job/job-error.ts";
import { UploadError } from "~/error/upload/upload-error.ts";
import { hashPassword } from "~/api/crypto.ts";
import { WORKER_TYPE } from "~/enum/worker-type.ts";
import type { AsyncZippable } from "fflate";

const CHUNK_LENGTH = 1_500_000; // 1.5 MB

const files: { [clientKey: string]: LufiFile } = {};
export const events: EventEmitter = new EventEmitter();

/**
 * Cancel an upload
 *
 * @param uploadJob
 * @returns
 */
export const cancel = (uploadJob: LufiJob): ResultAsync<LufiJob, Error> => {
  uploadJob.terminate();

  const job = new LufiJob(uploadJob.lufiFile, WORKER_TYPE.CANCEL);

  return ResultAsync.fromPromise(
    new Promise((resolve, reject) => {
      job
        .onMessage((event) => {
          if (event.data.event === EVENT.UPLOAD_CANCELLED) {
            files[job.lufiFile.keys.client].uploadStatus =
              UPLOAD_STATUS.CANCELED;

            resolve(job);
          }

          if (event.data.event === EVENT.OPERATION_FAILED) {
            reject(
              new JobError(
                "An error occured while trying to cancel an upload",
                { cause: event.data.error },
              ),
            );
          }
        })
        .requestMessage({
          args: {
            lufiFile: job.lufiFile,
          },
        });
    }),
    (error) => ensureError(error),
  );
};

/**
 * Return a list of files to be passed to compress()
 *
 * @param files
 * @param archiveEntries
 * @returns
 */
export const addFilesToArchive = (
  files: File[],
  archiveEntries: AsyncZippable = {},
): ResultAsync<AsyncZippable, Error> => {
  const asyncLoop = async (
    files: File[],
  ) => {
    for (const file of files) {
      const nameWithExtension = file.name.split("/")?.pop() || file.name;

      const nameWithoutExtension = nameWithExtension.split(".").shift();
      const extension = nameWithExtension.split(".").length > 1
        ? `.${nameWithExtension.split(".").pop()}`
        : "";

      let name = nameWithExtension;

      if (archiveEntries[name] !== undefined) {
        let i = 1;
        do {
          name = `${nameWithoutExtension}(${i})${extension}`;

          i++;
        } while (archiveEntries[name] !== undefined);
      }

      events.emit(EVENT.ARCHIVE_ADDED_FILE, { name, size: file.size });

      archiveEntries[name] = await file.bytes();
    }
  };

  return ResultAsync.fromPromise(
    asyncLoop(files),
    (error) => ensureError(error),
  ).andThen(() => okAsync(archiveEntries));
};

/**
 * Compress files into a zip
 *
 * @param archiveEntries
 * @param archiveName
 * @returns
 */
export const compress = (
  archiveEntries: AsyncZippable,
  archiveName: string,
): ResultAsync<LufiJob, Error> => {
  const lufiFile = new LufiFile(""); // Dummy Lufi File to avoid to check for LufiFile existance in other jobs
  const job = new LufiJob(lufiFile, WORKER_TYPE.COMPRESS);

  return okAsync(
    job
      .onMessage((event) => {
        if (event.data.event === EVENT.ARCHIVE_CREATED) {
          job.archiveFile = new File([event.data.buffer], archiveName, {
            type: "application/zip",
          });

          job.complete();
        }
      }).requestMessage({
        args: {
          lufiFile,
          archive: { entries: archiveEntries },
        },
      }),
  );
};

/**
 * Decompress a zip file
 *
 * @param zipFile
 * @returns
 */
export const decompress = (
  zipFile: File,
): ResultAsync<LufiJob, Error> => {
  const lufiFile = new LufiFile(""); // Dummy Lufi File to avoid to check for LufiFile existance in other jobs
  const job = new LufiJob(lufiFile, WORKER_TYPE.DECOMPRESS);

  return okAsync(
    job
      .onMessage((event) => {
        if (event.data.event === EVENT.ARCHIVE_DECOMPRESSED) {
          job.complete();
        }

        if (event.data.event === EVENT.ARCHIVE_RETRIEVED_FILE) {
          job.archiveFiles.push(
            new File([event.data.file.buffer], event.data.file.path),
          );
        }
      })
      .requestMessage({
        args: {
          lufiFile,
          archive: { file: zipFile },
        },
      }),
  );
};

/**
 * Handle logics of password hashing
 *
 * @param downloadUrl
 * @param password
 * @returns
 */
const handlePasswordHashing = (
  downloadUrl: URL,
  password?: string,
): ResultAsync<LufiFile, Error> => {
  if (password) {
    return fetchServerConfig(downloadUrl).andThen((config) => {
      // Password hashing on client side is only supported by recent versions of Lufi Server
      if (
        config.version.tag > "0.07.0"
      ) {
        const algo = isSecureContext()
          ? CryptoAlgorithm.WebCrypto
          : CryptoAlgorithm.Sjcl;

        return hashPassword(password, algo).andThen((hashedPassword) =>
          okAsync(LufiFile.fromDownloadUrl(downloadUrl, hashedPassword))
        );
      } else {
        return okAsync(LufiFile.fromDownloadUrl(downloadUrl, password));
      }
    });
  } else {
    return okAsync(LufiFile.fromDownloadUrl(downloadUrl));
  }
};

/**
 * Download a file from the server
 *
 * @param downloadUrl
 * @param password
 * @returns
 */
export const download = (
  downloadUrl: URL,
  password?: string,
): ResultAsync<LufiJob, Error> =>
  handlePasswordHashing(downloadUrl, password).andThen(
    (lufiFile: LufiFile) => {
      const job = new LufiJob(lufiFile, WORKER_TYPE.DOWNLOAD);

      return ResultAsync.fromPromise(
        new Promise((resolve, reject) => {
          const chunks: Blob[] = [];

          return job
            .onMessage((event) => {
              handleSocketResults(resolve, reject, job, event);

              if (event.data.event === EVENT.CHUNK_DOWNLOADED) {
                chunks.push(event.data.chunk.buffer);

                if (chunks.length >= 50) {
                  job.downloadedFile = new File(
                    job.downloadedFile
                      ? [job.downloadedFile.slice()].concat(chunks)
                      : chunks,
                    lufiFile.name,
                    {
                      type: lufiFile.type,
                    },
                  );

                  chunks.length = 0;
                }
              }

              if (event.data.event === EVENT.DOWNLOAD_COMPLETE) {
                job.downloadedFile = new File(
                  job.downloadedFile
                    ? [job.downloadedFile.slice()].concat(chunks)
                    : chunks,
                  lufiFile.name,
                  {
                    type: lufiFile.type,
                  },
                );

                chunks.length = 0;

                job.complete();
              }
            })
            .requestMessage({
              args: {
                lufiFile,
              },
            });
        }) as Promise<LufiJob>,
        (error) => new DownloadError(undefined, { cause: ensureError(error) }),
      );
    },
  );

/**
 * Retrieve informations about a file
 *
 * @param downloadUrl
 * @param password
 * @returns
 */
export const infos = (
  downloadUrl: URL,
  password?: string,
): ResultAsync<LufiJob, InfosError | CryptoError> =>
  handlePasswordHashing(downloadUrl, password).andThen((lufiFile) =>
    okAsync(new LufiJob(lufiFile, WORKER_TYPE.INFOS))
  )
    .andThen((job: LufiJob) =>
      ResultAsync.fromPromise(
        new Promise((resolve, reject) => {
          job
            .onMessage((event) => {
              if (event.data.event === EVENT.INFOS_RETRIEVED) {
                job.complete();
                resolve(job);
              }

              if (event.data.event === EVENT.OPERATION_FAILED) {
                reject(
                  new JobError(
                    "An error occured while trying to retrieve informations of the file",
                    { cause: event.data.error },
                  ),
                );
              }
            })
            .requestMessage({
              args: { lufiFile: job.lufiFile },
            });
        }) as Promise<LufiJob>,
        (error) => new InfosError(undefined, { cause: ensureError(error) }),
      )
    );

/**
 * Pause an upload/download job
 * @param job
 * @returns
 */
export const pause = (job: LufiJob): ResultAsync<LufiJob, Error> => {
  try {
    job.status = JobStatus.PAUSED;
    return okAsync(
      job.requestMessage({
        action: WORKER_ACTION.PAUSE,
        args: { lufiFile: job.lufiFile },
      }),
    );
  } catch (error) {
    return errAsync(
      new JobPauseError(undefined, { cause: ensureError(error) }),
    );
  }
};

/**
 * Remove a file uploaded on the server
 * @param removeUrl
 * @param password
 * @returns
 */
export const remove = (
  removeUrl: URL,
  password?: string,
): ResultAsync<LufiJob, Error> => {
  const lufiFile = LufiFile.fromRemoveUrl(removeUrl, password);
  const job = new LufiJob(lufiFile, WORKER_TYPE.REMOVE);

  return ResultAsync.fromPromise(
    new Promise((resolve, reject) => {
      job
        .onMessage((event) => {
          if (event.data.event === EVENT.FILE_REMOVED) {
            job.complete();
            resolve(job);
          }

          if (event.data.event === EVENT.OPERATION_FAILED) {
            reject(
              new JobError("An error occured while trying to remove a file", {
                cause: event.data.error,
              }),
            );
          }
        })
        .requestMessage({ args: { lufiFile } });
    }),
    (error) => ensureError(error),
  );
};

/**
 * Resume an upload/download job
 * @param job
 * @returns
 */
export const resume = (job: LufiJob): ResultAsync<LufiJob, Error> => {
  try {
    job.status = JobStatus.ONGOING;
    return okAsync(
      job.requestMessage({
        action: WORKER_ACTION.RESUME,
        args: { lufiFile: job.lufiFile },
      }),
    );
  } catch (error) {
    return errAsync(
      new JobResumeError(undefined, { cause: ensureError(error) }),
    );
  }
};

/**
 * Slice a file in multiple chunks
 *
 * @param chunkLength
 * @returns
 */
const sliceAndUpload = (
  job: LufiJob,
  file: File,
  algo: CryptoAlgorithm,
  chunkLength: number = CHUNK_LENGTH,
): ResultAsync<void, FileSlicingError> => {
  events.emit(EVENT.SLICE_STARTED, files[job.lufiFile.keys.client]);
  const totalChunks = Math.ceil(file.size / chunkLength) || 1;
  const concurrency = navigator.hardwareConcurrency || 1;

  files[job.lufiFile.keys.client].totalChunks = totalChunks;

  const sequentialLoop = async () => {
    for (let i = 0; i < totalChunks; i++) {
      const start = i * chunkLength;
      const end = Math.min(start + chunkLength, file.size);
      const buffer = await file.slice(start, end, file.type).arrayBuffer();

      job.requestMessage(
        {
          args: {
            chunk: {
              buffer,
              index: i,
            },
            lufiFile: files[job.lufiFile.keys.client],
            algo,
          },
        },
        [buffer],
      );

      if (i === 0) {
        const waitUntilUploadStarted = () =>
          new Promise((resolve) => {
            job.events.once(EVENT.UPLOAD_STARTED, () => {
              resolve(undefined);
            });
          });

        await waitUntilUploadStarted();
      } else if (i % concurrency === 0) {
        const waitForQueueAvailability = () =>
          new Promise((resolve) => {
            job.events.once(EVENT.CHUNK_UPLOADED, () => {
              resolve(undefined);
            });
          });

        await waitForQueueAvailability();
      }
    }
  };

  sequentialLoop();

  return okAsync(undefined);
};

/**
 * Start the upload on the server
 * @param serverUrl
 * @param file
 * @param delay
 * @param delAtFirstView
 * @param zipped
 * @param password
 * @param algo
 * @returns
 */
const startUpload = (
  serverUrl: URL,
  file: File,
  delay: number,
  delAtFirstView: boolean,
  zipped: boolean,
  password: string,
  algo: CryptoAlgorithm,
) =>
  crypto.generateKey(
    algo,
  ).andThen(
    (clientKey: string) => {
      if (password) {
        return hashPassword(password, algo).andThen((hashedPassword) =>
          okAsync({ password: hashedPassword, clientKey })
        );
      } else {
        return okAsync({ password, clientKey });
      }
    },
  ).andThen(({ password, clientKey }) => {
    files[clientKey] = new LufiFile(serverUrl.toString(), {
      delay,
      delAtFirstView,
      zipped,
      password,
      name: file.name.split("/").pop(), // Remove path from filename
      size: file.size,
      type: file.type,
      keys: { client: clientKey, server: "" },
    });

    const job = new LufiJob(files[clientKey], WORKER_TYPE.UPLOAD);

    files[clientKey].uploadStatus = UPLOAD_STATUS.QUEUED;

    return sliceAndUpload(job, file, algo).andThen(() =>
      ResultAsync.fromPromise(
        new Promise((resolve, reject) => {
          job.onMessage((event) => {
            handleSocketResults(resolve, reject, job, event);

            switch (event.data.event) {
              case EVENT.UPLOAD_COMPLETE:
                {
                  job.complete();
                  job.lufiFile.uploadStatus = UPLOAD_STATUS.COMPLETE;
                }
                break;
            }
          });
        }) as Promise<LufiJob>,
        (error) => ensureError(error),
      )
    );
  });

/**
 * Upload a file to the server
 *
 * @param serverUrl
 * @param filesToUpload
 * @param delay
 * @param delAtFirstView
 * @param zipped
 * @param zipName
 * @param password
 * @param algo
 * @returns
 */
export const upload = (
  serverUrl: URL,
  filesToUpload: File[],
  delay: number = 0,
  delAtFirstView: boolean = false,
  zipped: boolean = false,
  zipName: string = "documents.zip",
  password: string = "",
  algo: CryptoAlgorithm = CryptoAlgorithm.WebCrypto,
): ResultAsync<LufiJob[], Error> => {
  const operations: ResultAsync<LufiJob, Error>[] = [];

  if (!zipped) {
    filesToUpload.forEach((file) => {
      operations.push(startUpload(
        serverUrl,
        file,
        delay,
        delAtFirstView,
        zipped,
        password,
        algo,
      ));
    });
  } else {
    // If we just want to upload a single zip file (probably created by manually using compress())
    if (
      filesToUpload.length === 1 && filesToUpload[0].type === "application/zip"
    ) {
      operations.push(
        startUpload(
          serverUrl,
          filesToUpload[0],
          delay,
          delAtFirstView,
          zipped,
          password,
          algo,
        ),
      );
    } else {
      operations.push(
        addFilesToArchive(filesToUpload)
          .andThen((archiveEntries) =>
            compress(
              archiveEntries,
              zipName,
            )
              .andThen((job) => job.waitForCompletion())
              .andThen((job) => {
                if (job.archiveFile) {
                  return startUpload(
                    serverUrl,
                    job.archiveFile,
                    delay,
                    delAtFirstView,
                    true,
                    password,
                    algo,
                  );
                } else {
                  return errAsync(new JobError("archiveFile must be defined"));
                }
              })
          ),
      );
    }
  }

  return ResultAsync.combine(operations).orElse((error) =>
    errAsync(new UploadError(undefined, { cause: error }))
  );
};

/**
 * Handle socket opening operation in a promise
 *
 * @param resolve
 * @param reject
 * @param job
 * @param event
 */
const handleSocketResults = (
  resolve: {
    (value: LufiJob | PromiseLike<LufiJob>): void;
    (arg0: LufiJob): void;
  },
  reject: { (reason?: unknown): void; (arg0: unknown): void },
  job: LufiJob,
  event: MessageEvent,
) => {
  if (event.data.event === EVENT.SOCKET_OPENED) {
    resolve(job);
  }

  if (event.data.event === EVENT.OPERATION_FAILED) {
    reject(
      new JobError("The job returned an error", { cause: event.data.error }),
    );
  }
};

const getFilesQueued = (): LufiFile[] =>
  Object.values(files).filter((file) =>
    file.uploadStatus === UPLOAD_STATUS.QUEUED
  );

export const getFileIndexInQueue = (clientKey: string): number =>
  Object.keys(getFilesQueued()).indexOf(clientKey);
