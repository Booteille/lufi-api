import type { ServerDownloadChunkFailureMetadata } from "~/interface/server-download-chunk-failure-metadata.ts";
import type { ServerDownloadChunkSuccessMetadata } from "~/interface/server-download-chunk-success-metadata.ts";

export type ServerDownloadChunkMetadata =
  | ServerDownloadChunkSuccessMetadata
  | ServerDownloadChunkFailureMetadata;
