import type { WorkerActionMessage } from "~/interface/worker-action-message.ts";

export type WorkerRequestMessage = WorkerActionMessage | File | ArrayBuffer;
