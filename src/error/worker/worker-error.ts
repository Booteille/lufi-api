import { BaseError } from "~/error/base-error.ts";

export class WorkerError extends BaseError {}
