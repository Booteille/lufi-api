import { WorkerError } from "~/error/worker/worker-error.ts";

export class WorkerRemoveFailedError extends WorkerError {
  override message = "Failed to remove the file";
}
