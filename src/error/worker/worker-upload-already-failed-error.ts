import { WorkerError } from "~/error/worker/worker-error.ts";

export class WorkerUploadAlreadyFailedError extends WorkerError {
  override message = "File upload already failed";
}
