import { WorkerError } from "~/error/worker/worker-error.ts";

export class WorkerUndefinedParameterError extends WorkerError {
  override message = "Parameter must be defined";
}
