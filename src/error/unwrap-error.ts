import { BaseError } from "~/error/base-error.ts";

export class UnwrapError extends BaseError {
  override message: string = "The unwrapped result is an error";
}
