import { BaseError } from "~/error/base-error.ts";

export class ConnectionError extends BaseError {
  override message: string =
    "Unable to connect. Is the computer able to access the url?";
}
