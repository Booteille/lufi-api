import { FileOperationError } from "~/error/file/file-operation-error.ts";

export class FileSlicingError extends FileOperationError {
  override message: string = "An error occured while slicing a file";
}
