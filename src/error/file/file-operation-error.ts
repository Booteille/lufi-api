import { FileError } from "~/error/file/file-error.ts";

export class FileOperationError extends FileError {
  override message: string = "An error occured while operating on a file";
}
