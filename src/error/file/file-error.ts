import { BaseError } from "~/error/base-error.ts";

export class FileError extends BaseError {
  override message: string = "An error occured while dealing with a file";
}
