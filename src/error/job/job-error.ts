import { BaseError } from "~/error/base-error.ts";

export class JobError extends BaseError {}
