import { JobError } from "~/error/job/job-error.ts";

export class JobResumeError extends JobError {
  override message: string = "An error occured while trying to resume the job";
}
