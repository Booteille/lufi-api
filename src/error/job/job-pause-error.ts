import { JobError } from "~/error/job/job-error.ts";

export class JobPauseError extends JobError {
  override message: string = "An error occured while trying to pause the job";
}
