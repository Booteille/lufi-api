import { BaseError } from "~/error/base-error.ts";

export class ServerError extends BaseError {
  override message = "The server returned an error";
}
