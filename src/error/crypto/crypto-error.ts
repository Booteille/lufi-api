import { BaseError } from "~/error/base-error.ts";

export class CryptoError extends BaseError {}
