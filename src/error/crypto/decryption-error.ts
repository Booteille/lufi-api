import { CryptoError } from "~/error/crypto/crypto-error.ts";

export class DecryptionError extends CryptoError {
  override message: string = "Unable to decrypt the provided data";
}
