import { CryptoError } from "~/error/crypto/crypto-error.ts";

export class EncryptionError extends CryptoError {
  override message: string = "Unable to encrypt the provided data";
}
