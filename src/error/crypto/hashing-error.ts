import { CryptoError } from "~/error/crypto/crypto-error.ts";

export class HashingError extends CryptoError {
  override message: string = "Unable to hash the provided string";
}
