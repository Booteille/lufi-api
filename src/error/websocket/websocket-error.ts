import { BaseError } from "~/error/base-error.ts";

export class WebSocketError extends BaseError {}
