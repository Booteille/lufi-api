import { WebSocketError } from "~/error/websocket/websocket-error.ts";

export class WebSocketConnectionError extends WebSocketError {
  override message = "An error occured while trying to connect to WebSocket";
}
