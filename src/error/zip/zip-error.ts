import { BaseError } from "~/error/base-error.ts";

export class ZipError extends BaseError {}
