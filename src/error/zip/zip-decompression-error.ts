import { ZipError } from "~/error/zip/zip-error.ts";

export class ZipDecompressionError extends ZipError {
  override message = "An error occured while trying to decompress the data";
}
