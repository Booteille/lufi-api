import { ZipError } from "~/error/zip/zip-error.ts";

export class ZipCompressionError extends ZipError {
  override message = "An error occured while trying to compress the data";
}
