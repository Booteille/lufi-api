import { BaseError } from "~/error/base-error.ts";

export class DownloadError extends BaseError {
  override message: string = "An error occured while downloading the data";
}
