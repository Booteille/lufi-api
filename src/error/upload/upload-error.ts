import { BaseError } from "~/error/base-error.ts";

export class UploadError extends BaseError {
  override message: string = "An error occured while uploading the data";
}
