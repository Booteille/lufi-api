import { UploadError } from "~/error/upload/upload-error.ts";

export class UploadCancelError extends UploadError {
  override message: string =
    "An error occured while trying to cancel an upload";
}
