import { UploadError } from "~/error/upload/upload-error.ts";

export class UploadChunkError extends UploadError {
  override message: string = "An error occured while uploading the chunk";
}
