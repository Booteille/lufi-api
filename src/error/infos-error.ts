import { BaseError } from "~/error/base-error.ts";

export class InfosError extends BaseError {
  override message: string =
    "An error occured while trying to retrieve server informations";
}
