import { errAsync, ResultAsync } from "neverthrow";
import { ConnectionError } from "~/error/connection-error.ts";
import { ServerError } from "~/error/server-error.ts";
import type { ServerConfig } from "~/interface/server-config.ts";

/**
 * Ensure an error message is transformed in an Error object
 *
 * @param value
 * @returns
 */
export const ensureError = (value: unknown): Error => {
  if (value instanceof Error) return value;

  let stringified = "[Unable to stringify the thrown value]";
  try {
    stringified = JSON.stringify(value);
  } catch (_error) {
    /* empty */
  }

  return new Error(stringified);
};

/**
 * Retrieve Lufi's config from its API
 *
 * @param instanceUrl
 * @returns
 */
export const fetchServerConfig = (
  instanceUrl: URL,
): ResultAsync<ServerConfig, Error> => {
  const originMatches = instanceUrl.href.match(
    /(.*?)\/?(?:\/[dr]{1}\/|login\/?|files\/?)/,
  );

  const urlOrigin = originMatches && originMatches[1]
    ? originMatches[1]
    : instanceUrl.origin;

  return ResultAsync.fromPromise(
    fetch(urlOrigin + "/about/config"),
    (error) =>
      new ConnectionError(undefined, {
        cause: ensureError(error),
      }),
  ).andThen((response) => {
    if (response.ok) {
      return ResultAsync.fromPromise(
        response.json(),
        (error) => ensureError(error),
      );
    } else {
      return errAsync(
        new ServerError(undefined, { context: response.statusText }),
      );
    }
  });
};

export const isDenoRuntime = (): boolean => typeof Deno !== "undefined";

export const isSecureContext = (): boolean => {
  return isDenoRuntime() || globalThis.isSecureContext ||
    globalThis.location.protocol === "https:";
};

export const workerUrl = (relativePath: string): URL => {
  return isDenoRuntime()
    ? new URL(`./worker/${relativePath}.ts`, new URL(".", import.meta.url).href)
    : new URL(
      import.meta.resolve(
        `./${
          relativePath !== "encrypt" ? `worker/${relativePath}` : relativePath
        }.js`,
      ),
    );
};
