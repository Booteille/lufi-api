import type { ResultAsync } from "neverthrow";
import { EVENT } from "~/enum/event.ts";
import { events, init, sendFileError, updateFile } from "~/worker/shared.ts";
import type { WorkerActionMessage } from "~/interface/worker-action-message.ts";
import { downloadChunk } from "~/api/websocket.ts";
import PQueue from "p-queue";
import { WORKER_ACTION } from "~/enum/worker-action.ts";
import type { WebSocketError } from "~/error/websocket/websocket-error.ts";

declare const self: Worker;

const QUEUE_CONCURRENCY_LIMIT = navigator.hardwareConcurrency || 1;
const queue = new PQueue({
  concurrency: QUEUE_CONCURRENCY_LIMIT,
});
let itemsInQueue = 0;
let isInitiated = false;
let isPaused = false;

self.onmessage = (event: MessageEvent) => {
  if (!isInitiated) {
    init();
    isInitiated = true;

    events.once(EVENT.DOWNLOAD_STARTED, () => {
      self.postMessage({ event: EVENT.DOWNLOAD_STARTED });
    });

    events.once(EVENT.DOWNLOAD_COMPLETE, () => {
      self.postMessage({ event: EVENT.DOWNLOAD_COMPLETE });
    });

    events.on(EVENT.FILE_UPDATED, updateFile);

    events.on(
      EVENT.CHUNK_DOWNLOADED,
      (buffer: ArrayBuffer, index: number) => {
        itemsInQueue--;

        if (!isPaused && itemsInQueue < QUEUE_CONCURRENCY_LIMIT) {
          queue.start();
        }

        self.postMessage({
          event: EVENT.CHUNK_DOWNLOADED,
          chunk: { buffer, index },
        }, [buffer]);
      },
    );
  }

  const data = event.data as WorkerActionMessage;

  switch (data.action) {
    case WORKER_ACTION.PAUSE:
      {
        isPaused = true;
        self.postMessage({ event: EVENT.JOB_PAUSED });
      }
      break;

    case WORKER_ACTION.RESUME:
      {
        isPaused = false;
        self.postMessage({ event: EVENT.JOB_RESUMED });
      }
      break;

    default:
      download(data).mapErr((error) => {
        sendFileError(data.args.lufiFile, error);
      });
  }
};

const download = (
  workerMessage: WorkerActionMessage,
): ResultAsync<void, WebSocketError> => {
  const { lufiFile } = workerMessage.args;

  events.on(EVENT.DOWNLOAD_STARTED, async () => {
    for (let i = 1; i < lufiFile.totalChunks; i++) {
      if (!isPaused && itemsInQueue < QUEUE_CONCURRENCY_LIMIT) {
        queue.start();
      }

      await queue.add(async () => {
        await downloadChunk(lufiFile, i);

        itemsInQueue++;

        if (isPaused || itemsInQueue === QUEUE_CONCURRENCY_LIMIT) {
          queue.pause();
        }
      });
    }
  });

  itemsInQueue++;

  return downloadChunk(lufiFile, 0);
};
