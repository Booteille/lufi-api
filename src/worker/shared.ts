import EventEmitter from "events";
import { WORKER_ACTION } from "~/enum/worker-action.ts";
import type { LufiFile } from "~/entities/lufi-file.ts";
import type { WorkerActionMessage } from "~/interface/worker-action-message.ts";
import { EVENT } from "~/enum/event.ts";
import { UPLOAD_STATUS } from "~/enum/file-status.ts";
import type { WorkerEvent } from "~/interface/worker-event.ts";

declare let self: Worker;

export const events = new EventEmitter();

/**
 * Update file in workers and provide modifications to the main thread
 *
 * @param lufiFile
 * @param args
 * @returns
 */
export const updateFile = (lufiFile: LufiFile, args: Partial<LufiFile>) => {
  Object.assign(lufiFile, args);

  if (typeof WorkerGlobalScope !== "undefined") {
    self.postMessage({
      event: EVENT.FILE_UPDATED,
      lufiFile,
    } as WorkerEvent);
  }

  return lufiFile;
};

export const sendFileError = (lufiFile: LufiFile, error: Error) => {
  updateFile(lufiFile, { uploadStatus: UPLOAD_STATUS.FAILED });

  self.postMessage({ event: EVENT.OPERATION_FAILED, error } as WorkerEvent);
};

/**
 * Init function to be called at the beginning of each child worker's onmessage event.
 */
export const init = () => {
  events.once(EVENT.SOCKET_OPENED, () => {
    self.postMessage({
      event: EVENT.SOCKET_OPENED,
    });
  });

  events.once(EVENT.OPERATION_FAILED, (error: Error) => {
    self.postMessage({ event: EVENT.OPERATION_FAILED, error });
  });
};

export const isWorkerActionMessage = (
  // deno-lint-ignore no-explicit-any
  message: any,
): message is WorkerActionMessage => {
  return (
    typeof message === "object" &&
    message !== null &&
    "action" in message &&
    Object.values(WORKER_ACTION).includes(message.action)
  );
};
