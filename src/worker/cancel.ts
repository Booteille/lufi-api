import { EVENT } from "~/enum/event.ts";
import { events, init } from "~/worker/shared.ts";
import { cancelUpload as wsCancelUpload } from "~/api/websocket.ts";
import type { WorkerActionMessage } from "~/interface/worker-action-message.ts";
import type { ResultAsync } from "neverthrow";
import type { WorkerEvent } from "~/interface/worker-event.ts";
import type { WebSocketError } from "~/error/websocket/websocket-error.ts";

declare const self: Worker;
let isInitiated = false;

self.onmessage = (event: MessageEvent) => {
  if (!isInitiated) {
    init();
    isInitiated = true;
  }

  events.on(EVENT.UPLOAD_CANCELLED, (success: boolean) => {
    self.postMessage({ event: EVENT.UPLOAD_CANCELLED, success });
  });

  cancelUpload(event.data).mapErr((error) => {
    self.postMessage({
      event: EVENT.OPERATION_FAILED,
      error,
    } as WorkerEvent);
  });
};

const cancelUpload = (
  workerMessage: WorkerActionMessage,
): ResultAsync<void, WebSocketError> =>
  wsCancelUpload(workerMessage.args.lufiFile);
