import { errAsync, okAsync, type ResultAsync } from "neverthrow";
import { EVENT } from "~/enum/event.ts";
import { uploadChunk } from "~/api/websocket.ts";
import { getFileIndexInQueue } from "~/api/lufi.ts";
import type { WorkerActionMessage } from "~/interface/worker-action-message.ts";
import { events, init, sendFileError, updateFile } from "~/worker/shared.ts";
import PQueue from "p-queue";
import { WORKER_ACTION } from "~/enum/worker-action.ts";
import { encrypt } from "~/api/crypto.ts";
import { WorkerUndefinedParameterError } from "~/error/worker/worker-undefined-parameter-error.ts";
import type { LufiFile } from "~/entities/lufi-file.ts";
import { workerUrl } from "~/utils.ts";

declare const self: Worker;

let isInitialized = false;
const QUEUE_CONCURRENCY_LIMIT = navigator.hardwareConcurrency || 1;
const queue = new PQueue({
  concurrency: QUEUE_CONCURRENCY_LIMIT,
  autoStart: false,
});
let itemsInQueue = 0;
let isPaused = false;
let lufiFile: LufiFile;
const encryptJob = new Worker(workerUrl("encrypt"), { type: "module" });

self.onmessage = (event: MessageEvent) => {
  if (!isInitialized) {
    init();
    isInitialized = true;

    lufiFile = event.data.args.lufiFile;

    events.once(EVENT.UPLOAD_STARTED, () => {
      self.postMessage({ event: EVENT.UPLOAD_STARTED });
    });

    events.on(EVENT.CHUNK_UPLOADED, () => {
      self.postMessage({ event: EVENT.CHUNK_UPLOADED });

      itemsInQueue--;

      if (
        queue.isPaused && !isPaused && itemsInQueue < QUEUE_CONCURRENCY_LIMIT
      ) {
        resumeQueue();
      }
    });

    events.once(EVENT.UPLOAD_COMPLETE, () => {
      self.postMessage({
        event: EVENT.UPLOAD_COMPLETE,
      });
    });

    events.on(EVENT.FILE_UPDATED, updateFile);
  }

  if (event.data.args.chunk) {
    event.data.args.lufiFile = lufiFile;
    return startUpload(
      event.data,
      getFileIndexInQueue(lufiFile.keys.client),
    ).mapErr((error) => {
      sendFileError(lufiFile, error);
    });
  } else {
    if (event.data.action === WORKER_ACTION.PAUSE) {
      isPaused = true;
      pauseQueue();
    } else if (event.data.action === WORKER_ACTION.RESUME) {
      isPaused = false;
      resumeQueue();
    } else {
      sendFileError(lufiFile, new WorkerUndefinedParameterError());
    }
  }
};

const startUpload = (
  workerMessage: WorkerActionMessage,
  serverQueueIndex: number,
): ResultAsync<void, Error> => {
  const { lufiFile, algo } = workerMessage.args;

  if (
    workerMessage.args.chunk !== undefined &&
    algo !== undefined
  ) {
    // Upload first chunk before to get serverKey in order to be able to upload other chunks
    if (workerMessage.args.chunk.index === 0) {
      encrypt(lufiFile.keys.client, workerMessage.args.chunk.buffer, algo).map(
        (encryptedData) => {
          uploadChunk(
            lufiFile,
            {
              total: lufiFile.totalChunks,
              part: 0,
              size: lufiFile.size,
              name: lufiFile.name,
              type: lufiFile.type,
              delay: lufiFile.delay,
              del_at_first_view: lufiFile.delAtFirstView,
              zipped: lufiFile.zipped,
              id: null,
              i: serverQueueIndex,
              file_pwd: lufiFile.password,
            },
            encryptedData,
          );

          itemsInQueue++;
        },
      );
    } else {
      queue.add(() => {
        if (workerMessage.args.chunk) {
          const waitForEncryption = () => {
            itemsInQueue++;

            return new Promise((resolve) => {
              encryptJob.onmessage = (event) => {
                resolve(uploadChunk(
                  lufiFile,
                  {
                    total: lufiFile.totalChunks,
                    part: event.data.chunkIndex,
                    size: lufiFile.size,
                    name: lufiFile.name,
                    type: lufiFile.type,
                    delay: lufiFile.delay,
                    del_at_first_view: lufiFile.delAtFirstView,
                    zipped: lufiFile.zipped,
                    id: lufiFile.keys.server,
                    i: serverQueueIndex,
                    file_pwd: lufiFile.password,
                  },
                  event.data.encryptedData,
                ));
              };
            });
          };

          encryptJob.postMessage(workerMessage, [
            workerMessage.args.chunk.buffer,
          ]);

          if (
            !queue.isPaused && (isPaused ||
              itemsInQueue === QUEUE_CONCURRENCY_LIMIT)
          ) {
            pauseQueue();
          }

          waitForEncryption();
        }
      });
    }
  } else {
    return errAsync(
      new WorkerUndefinedParameterError(),
    );
  }
  return okAsync(undefined);
};

const pauseQueue = () => {
  queue.pause();
  self.postMessage({ event: EVENT.JOB_PAUSED });
};

const resumeQueue = () => {
  queue.start();
  self.postMessage({ event: EVENT.JOB_RESUMED });
};
