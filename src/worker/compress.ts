import { ZipCompressionError } from "~/error/zip/zip-compression-error.ts";
import { zip } from "fflate";
import { errAsync, okAsync, ResultAsync } from "neverthrow";
import { WorkerUndefinedParameterError } from "~/error/worker/worker-undefined-parameter-error.ts";
import { ensureError } from "~/utils.ts";
import type { WorkerActionMessage } from "~/interface/worker-action-message.ts";
import { EVENT } from "~/enum/event.ts";

declare const self: Worker;

self.onmessage = (event: MessageEvent) => {
  compress(event.data).map((bytes) => {
    self.postMessage({ event: EVENT.ARCHIVE_CREATED, buffer: bytes.buffer }, [
      bytes.buffer,
    ]);
  }).mapErr((error) => {
    self.postMessage({ event: EVENT.OPERATION_FAILED, error });
  });
};

const compress = (
  workerMessage: WorkerActionMessage,
): ResultAsync<Uint8Array, Error> => {
  const { archive } = workerMessage.args;

  if (archive) {
    const promiseZip = (): Promise<Uint8Array> =>
      new Promise((resolve, reject) => {
        if (archive.entries) {
          zip(archive.entries, (error, data) => {
            if (error) reject(error);

            resolve(data);
          });
        } else {
          reject(
            new WorkerUndefinedParameterError(
              "archive.entries must be defined",
            ),
          );
        }
      });
    return ResultAsync.fromPromise(
      promiseZip(),
      (error) => error,
    ).andThen((bytes) => okAsync(bytes)).orElse((error) =>
      errAsync(
        new ZipCompressionError(undefined, { cause: ensureError(error) }),
      )
    );
  } else {
    return errAsync(
      new WorkerUndefinedParameterError("archive must be defined"),
    );
  }
};
