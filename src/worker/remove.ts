import { EVENT } from "~/enum/event.ts";
import type { WorkerActionMessage } from "~/interface/worker-action-message.ts";
import { ResultAsync } from "neverthrow";
import type { WorkerEvent } from "~/interface/worker-event.ts";
import { ensureError } from "~/utils.ts";
import { init } from "~/worker/shared.ts";
import { WorkerRemoveFailedError } from "~/error/worker/worker-remove-failed-error.ts";

declare const self: Worker;

let isInitiated = false;

self.onmessage = (event: MessageEvent) => {
  if (!isInitiated) {
    init();
    isInitiated = true;
  }

  removeFile(event.data)
    .map(() => {
      self.postMessage({ event: EVENT.FILE_REMOVED });
    })
    .mapErr((error) => {
      self.postMessage({
        event: EVENT.OPERATION_FAILED,
        error,
      } as WorkerEvent);
    });
};

const removeFile = (
  workerMessage: WorkerActionMessage,
): ResultAsync<void, Error> =>
  ResultAsync.fromPromise(
    new Promise((resolve, reject) => {
      const lufiFile = workerMessage.args.lufiFile;

      fetch(
        `${
          new URL(lufiFile.serverUrl).href
        }d/${lufiFile.keys.server}/${lufiFile.actionToken}`,
      ).then((
        response,
      ) => (response.ok ? resolve(undefined) : reject(
        new WorkerRemoveFailedError(undefined, {
          cause: ensureError(response.statusText),
        }),
      )));
    }),
    (error) => ensureError(error),
  );
