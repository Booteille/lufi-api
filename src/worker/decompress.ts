import { ZipDecompressionError } from "~/error/zip/zip-decompression-error.ts";
import { unzip, type Unzipped } from "fflate";
import { errAsync, okAsync, ResultAsync } from "neverthrow";
import { WorkerUndefinedParameterError } from "~/error/worker/worker-undefined-parameter-error.ts";
import { ensureError } from "~/utils.ts";
import type { WorkerActionMessage } from "~/interface/worker-action-message.ts";
import { EVENT } from "~/enum/event.ts";

declare const self: Worker;

self.onmessage = (event: MessageEvent) => {
  decompress(event.data).map(() => {
    self.postMessage({ event: EVENT.ARCHIVE_DECOMPRESSED });
  }).mapErr((error) => {
    self.postMessage({ event: EVENT.OPERATION_FAILED, error });
  });
};

const decompress = (
  workerMessage: WorkerActionMessage,
): ResultAsync<void, Error> => {
  const { archive } = workerMessage.args;

  if (archive !== undefined) {
    const unzipPromise = async (): Promise<Unzipped> => {
      if (archive.file) {
        const fileBytes = await archive.file.bytes();

        return new Promise((resolve, reject) => {
          unzip(fileBytes, (error, files) => {
            if (error) reject(error);

            resolve(files);
          });
        });
      } else {
        return Promise.reject(
          new WorkerUndefinedParameterError("archive.file must be defined"),
        );
      }
    };

    return ResultAsync.fromPromise(unzipPromise(), (error) => error)
      .andThen((files) => {
        try {
          for (const path in files) {
            self.postMessage({
              event: EVENT.ARCHIVE_RETRIEVED_FILE,
              file: { buffer: files[path].buffer, path },
            }, [files[path].buffer]);
          }
          return okAsync(undefined);
        } catch (error) {
          return errAsync(error);
        }
      }).orElse((error) =>
        errAsync(
          new ZipDecompressionError(undefined, { cause: ensureError(error) }),
        )
      );
  } else {
    return errAsync(
      new WorkerUndefinedParameterError("archive must be defined"),
    );
  }
};
