import type { ResultAsync } from "neverthrow";
import { downloadChunk, downloadSocketUrl, sockets } from "~/api/websocket.ts";
import { EVENT } from "~/enum/event.ts";
import type { WorkerActionMessage } from "~/interface/worker-action-message.ts";
import type { WorkerEvent } from "~/interface/worker-event.ts";
import { events, init } from "~/worker/shared.ts";
import type { WebSocketError } from "~/error/websocket/websocket-error.ts";

declare const self: Worker;
let isInitiated = false;

self.onmessage = (event: MessageEvent) => {
  if (!isInitiated) {
    init();
    isInitiated = true;
  }

  events.on(EVENT.DOWNLOAD_STARTED, () => {
    sockets[downloadSocketUrl(event.data.args.lufiFile)].close();

    self.postMessage({ event: EVENT.INFOS_RETRIEVED });
  });

  retrieveInfos(event.data).mapErr((error) => {
    self.postMessage({
      event: EVENT.OPERATION_FAILED,
      error,
    } as WorkerEvent);
  });
};

const retrieveInfos = (
  workerMessage: WorkerActionMessage,
): ResultAsync<void, WebSocketError> =>
  downloadChunk(workerMessage.args.lufiFile, 0);
