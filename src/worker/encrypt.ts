import { encrypt } from "~/api/crypto.ts";
import { UPLOAD_STATUS } from "~/enum/file-status.ts";
import type { EncryptionError } from "~/error/crypto/encryption-error.ts";
import { init } from "~/worker/shared.ts";
import type { WorkerActionMessage } from "~/interface/worker-action-message.ts";
import { WorkerUndefinedParameterError } from "~/error/worker/worker-undefined-parameter-error.ts";
import { WorkerUploadAlreadyFailedError } from "~/error/worker/worker-upload-already-failed-error.ts";

declare const self: Worker;

let isInitiated = false;

self.onmessage = (event: MessageEvent) => {
  if (!isInitiated) {
    init();
    isInitiated = true;
  }

  const { lufiFile, chunk, algo } = (event.data as WorkerActionMessage).args;

  if (chunk && typeof algo !== "undefined") {
    if (lufiFile.uploadStatus !== UPLOAD_STATUS.FAILED) {
      encrypt(lufiFile.keys.client, chunk.buffer, algo)
        .map((encryptedData) => {
          self.postMessage({ encryptedData, chunkIndex: chunk.index }, [
            typeof encryptedData.data === "string"
              ? new TextEncoder().encode(encryptedData.data).buffer
              : encryptedData.data,
          ]);
        })
        .mapErr((error: EncryptionError) => {
          self.postMessage({ error });
        });
    } else {
      self.postMessage({
        error: new WorkerUploadAlreadyFailedError("File upload already failed"),
      });
    }
  } else {
    if (!chunk) {
      self.postMessage({
        error: new WorkerUndefinedParameterError(
          "chunk buffer must be defined",
        ),
      });
    }

    if (typeof algo === "undefined") {
      self.postMessage({
        error: new WorkerUndefinedParameterError("algo must be defined"),
      });
    }
  }
};
