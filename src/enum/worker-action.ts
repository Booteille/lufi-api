export enum WORKER_ACTION {
  PAUSE,
  PROVIDE_FILE,
  RESUME,
}
