export enum SocketPath {
  ROOT = "/",
  UPLOAD = "upload",
  DOWNLOAD = "download",
}
