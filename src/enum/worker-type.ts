export enum WORKER_TYPE {
  CANCEL,
  COMPRESS,
  DECOMPRESS,
  DOWNLOAD,
  INFOS,
  REMOVE,
  UPLOAD,
}
