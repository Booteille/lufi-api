export enum JobStatus {
  COMPLETE,
  FAILED,
  ONGOING,
  PAUSED,
}
