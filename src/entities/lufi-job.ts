import EventEmitter from "events";
import { errAsync, okAsync, ResultAsync } from "neverthrow";
import type { LufiFile } from "~/entities/lufi-file.ts";
import { EVENT } from "~/enum/event.ts";
import { JobStatus } from "~/enum/job-status.ts";
import type { WorkerRequestMessage } from "~/type/worker-request-message.ts";
import { ensureError, workerUrl } from "~/utils.ts";
import { UPLOAD_STATUS } from "~/enum/file-status.ts";
import { WORKER_TYPE } from "~/enum/worker-type.ts";

export class LufiJob {
  public events: EventEmitter = new EventEmitter();
  public lufiFile: LufiFile;
  public status = JobStatus.ONGOING;
  public archiveFile: File | undefined;
  public archiveFiles: File[] = [];
  public downloadedFile: File | undefined;
  public worker: Worker;

  private isTerminated = false;

  constructor(lufiFile: LufiFile, workerType: WORKER_TYPE) {
    switch (workerType) {
      case WORKER_TYPE.CANCEL:
        {
          this.worker = new Worker(workerUrl("cancel"), { type: "module" });
        }
        break;

      case WORKER_TYPE.COMPRESS:
        {
          this.worker = new Worker(workerUrl("compress"), { type: "module" });
        }
        break;

      case WORKER_TYPE.DECOMPRESS:
        {
          this.worker = new Worker(workerUrl("decompress"), { type: "module" });
        }
        break;

      case WORKER_TYPE.DOWNLOAD:
        {
          this.worker = new Worker(workerUrl("download"), { type: "module" });
        }
        break;

      case WORKER_TYPE.INFOS:
        {
          this.worker = new Worker(workerUrl("infos"), { type: "module" });
        }
        break;

      case WORKER_TYPE.REMOVE:
        {
          this.worker = new Worker(workerUrl("remove"), { type: "module" });
        }
        break;

      case WORKER_TYPE.UPLOAD:
        {
          this.worker = new Worker(workerUrl("upload"), { type: "module" });
        }
        break;
    }

    this.lufiFile = lufiFile;
    this.events.once(EVENT.JOB_TERMINATED, () => {
      this.isTerminated = true;
      this.terminate();
    });

    this.events.once(EVENT.OPERATION_FAILED, (error: Error) => {
      this.status = JobStatus.FAILED;
      this.lufiFile.uploadStatus = UPLOAD_STATUS.FAILED;

      this.events.emit(EVENT.JOB_TERMINATED, error);
    });

    this.onError((event) => console.error(event.error));
  }

  /**
   * Tells the worker the job is complete
   */
  public complete = (): void => {
    this.status = JobStatus.COMPLETE;
    this.events.emit(EVENT.JOB_TERMINATED);
  };

  public hasFailed = () => this.status === JobStatus.FAILED;

  public onError = (callback: (arg: ErrorEvent) => void): LufiJob => {
    this.worker.onerror = (event) => {
      callback(event);
    };

    return this;
  };

  public onMessage = (callback?: (arg: MessageEvent) => void): LufiJob => {
    this.worker.onmessage = (e) => {
      if (callback) {
        callback(e);
      }

      const event = e.data.event;

      if (event) {
        if (event === EVENT.FILE_UPDATED) {
          Object.assign(this.lufiFile, e.data.lufiFile);
        } else {
          this.dispatchEvent(event, e.data.error);
        }
      }
    };

    return this;
  };

  public onMessageError = (callback: (arg: MessageEvent) => void): LufiJob => {
    this.worker.onmessageerror = (event) => {
      callback(event);
    };

    return this;
  };

  public onProgress = (callback: () => void): LufiJob => {
    this.events.on(EVENT.CHUNK_UPLOADED, () => {
      callback();
    });
    this.events.on(EVENT.CHUNK_DOWNLOADED, () => {
      callback();
    });

    return this;
  };

  public requestMessage = (
    msg: WorkerRequestMessage,
    transferable: Transferable[] = [],
  ): LufiJob => {
    this.worker.postMessage(msg, transferable);

    return this;
  };

  public terminate = (): LufiJob => {
    this.worker.terminate();

    return this;
  };

  public waitForCompletion = (): ResultAsync<LufiJob, Error> => {
    if (this.isTerminated) {
      if (this.status === JobStatus.COMPLETE) {
        return okAsync(this);
      } else {
        return errAsync(ensureError("Job has failed"));
      }
    } else {
      return ResultAsync.fromPromise(
        new Promise((resolve, reject) => {
          this.events.once(EVENT.OPERATION_FAILED, (error: Error) => {
            reject(error);
          });

          this.events.once(EVENT.JOB_TERMINATED, () => {
            if (this.status === JobStatus.COMPLETE) {
              resolve(this);
            }
          });
        }),
        (error) => ensureError(error),
      );
    }
  };

  public waitForStart = (): ResultAsync<LufiJob, Error> =>
    ResultAsync.fromPromise(
      new Promise((resolve, reject) => {
        this.events.once(EVENT.OPERATION_FAILED, (error: Error) => {
          reject(error);
        });

        this.events.once(EVENT.UPLOAD_STARTED, () => {
          resolve(this);
        });

        this.events.once(EVENT.DOWNLOAD_STARTED, () => {
          resolve(this);
        });
      }),
      (error) => ensureError(error),
    );

  private dispatchEvent = (event: EVENT, error: undefined | Error) => {
    this.events.emit(event, error);
  };
}
