import { UPLOAD_STATUS } from "~/enum/file-status.ts";

export class LufiFile {
  public actionToken: string = "";
  public chunksReady: number = 0;
  public createdAt: number = 0;
  public delay: number = 0;
  public delAtFirstView: boolean = false;
  public keys: { client: string; server: string } = { client: "", server: "" };
  public name: string = "";
  public password = "";
  public queueIndex: number = 0;
  public serverUrl: string;
  public size: number = 0;
  public uploadStatus: UPLOAD_STATUS = UPLOAD_STATUS.INITIALIZED;
  public totalChunks: number = 0;
  public type: string = "";
  public zipped: boolean = false;

  constructor(serverUrl: string, properties?: Partial<LufiFile>) {
    this.serverUrl = serverUrl;

    Object.assign(this, properties);
  }

  public downloadUrl(): URL {
    const serverUrl = new URL(this.serverUrl);

    return new URL(
      `${
        serverUrl.origin + serverUrl.pathname
      }r/${this.keys.server}#${this.keys.client}`,
    );
  }

  public removeUrl(): URL {
    const serverUrl = new URL(this.serverUrl);

    return new URL(
      `${
        serverUrl.origin + serverUrl.pathname
      }d/${this.keys.server}/${this.actionToken}`,
    );
  }

  static fromDownloadUrl(downloadUrl: URL, password = ""): LufiFile {
    const pathinfos = downloadUrl.pathname.split("r/");

    const keys = {
      client: downloadUrl.hash.slice(1).split("&")[0],
      server: pathinfos[1],
    }; // split("&") is here to remove parameters like &utm_source from URL, sometimes added automatically by third parties.

    return new LufiFile(downloadUrl.origin + pathinfos[0], {
      keys,
      password,
    });
  }

  static fromRemoveUrl(removeUrl: URL, password = ""): LufiFile {
    const pathInfos = removeUrl.pathname.split("d/");
    const splittedPath = pathInfos[1].split("/");

    const keys = { client: "", server: splittedPath[0] };

    return new LufiFile(removeUrl.origin + pathInfos[0], {
      keys,
      password,
      actionToken: splittedPath[1],
    });
  }
}
