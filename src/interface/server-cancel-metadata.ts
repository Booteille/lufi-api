export interface ServerCancelMetadata {
  action: string;
  i: number;
  msg: string;
  success: boolean;
}
