import type { WebSocketCancelArguments } from "~/interface/websocket-cancel-arguments.ts";
import type { WebSocketDownloadArguments } from "~/interface/websocket-download-arguments.ts";
import type { WebSocketUploadArguments } from "~/interface/websocket-upload-arguments.ts";

export type WebSocketArguments =
  | WebSocketCancelArguments
  | WebSocketUploadArguments
  | WebSocketDownloadArguments;
