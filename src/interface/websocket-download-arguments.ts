export interface WebSocketDownloadArguments {
  parts: ArrayBuffer[];
  clientKey: string;
  password?: string;
}
