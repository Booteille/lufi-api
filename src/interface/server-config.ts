export interface ServerConfig {
  allow_pwd_on_files: number;
  broadcast_message: string | null;
  default_delay: number;
  delay_for_size: number | null;
  force_burn_after_reading: number;
  instance_name: string;
  keep_ip_during: number;
  max_delay: number;
  max_file_size: number | null;
  need_authentication: boolean;
  report: string;
  stop_upload: boolean;
  version: {
    commit: string;
    tag: string;
  };
}
