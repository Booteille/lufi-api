/**
 * @description Names are those provided by the server. Aliases correspond to the API equivalent
 */
export interface ServerUploadChunkMetadata {
  created_at: number;

  /**
   * @alias delAtFirstView
   */
  del_at_first_view: boolean;

  delay: number;

  duration: number;

  /**
   * @alias queueIndex
   */
  i: number;

  /**
   * Chunk/part number
   */
  j: number;

  msg?: string;

  name: string;

  /**
   * @alias chunks
   */
  parts: number;

  sent_delay: number;

  /**
   * @alias keys.server
   */
  short: string;

  size: number;

  success: boolean;

  /**
   * @alias actionToken
   */
  token: string;
}
