import type { WORKER_ACTION } from "~/enum/worker-action.ts";
import type { LufiFile } from "~/entities/lufi-file.ts";
import type { CryptoAlgorithm } from "~/enum/crypto-algorithm.ts";
import type { AsyncZippable } from "fflate";

export interface WorkerActionMessage {
  action?: WORKER_ACTION;
  args: {
    lufiFile: LufiFile;
    archive?: {
      entries?: AsyncZippable;
      file?: File;
    };
    chunk?: {
      index: number;
      buffer: ArrayBuffer;
    };
    algo?: CryptoAlgorithm;
  };
}
