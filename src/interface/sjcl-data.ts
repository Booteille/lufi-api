import type { SjclCiphers } from "lufi-sjcl";

export interface SjclData {
  adata: string;
  cipher: SjclCiphers;
  ct: string;
  iter: number;
  iv: string;
  ks: number;
  mode: string;
  salt: string;
  ts: number;
  v: number;
}
