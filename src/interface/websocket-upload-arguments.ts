export interface WebSocketUploadArguments {
  serverUrl: URL;
  file: File;
  delay: number;
  delAtFirstView: boolean;
  zipped: boolean;
  password: string;
  clientKey: string;
  parts: Blob[];
}
