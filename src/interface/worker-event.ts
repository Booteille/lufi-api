import type { EVENT } from "~/enum/event.ts";

export interface WorkerEvent {
  event: EVENT;
  error?: Error;
}
