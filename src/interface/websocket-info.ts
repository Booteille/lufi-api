export interface WebSocketInfo {
  url: string;
  key: string;
}
