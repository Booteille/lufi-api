import type { CHUNK_STATUS } from "~/enum/chunk-status.ts";

export interface Chunk {
  blob: Blob;
  status: CHUNK_STATUS;
}
