import type { CryptoAlgorithm } from "~/enum/crypto-algorithm.ts";

export interface EncryptedData {
  algo: CryptoAlgorithm;
  data: string | ArrayBuffer;
  iv: string | Uint8Array;
}
