export interface ServerDownloadChunkFailureMetadata {
  msg: string;
  success: boolean;
}
