/**
 * @description Names are those needed by the server. Aliases correspond to the API equivalent
 */
export interface ClientUploadChunkMetadata {
  /**
   * @alias delAtFirstView
   */
  del_at_first_view: boolean;

  delay: number;

  /**
   * @alias password
   */
  file_pwd?: string;

  /**
   * @alias queueIndex
   */
  i: number;

  /**
   * @alias keys.server
   */
  id: string | null;

  name: string;

  /**
   * @alias chunk
   */
  part: number;

  size: number;

  /**
   * @alias totalChunks
   */
  total: number;

  type: string;

  zipped: boolean;
}
