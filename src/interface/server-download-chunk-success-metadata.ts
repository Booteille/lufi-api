/**
 * @description Names are those provided by the server. Aliases correspond to the API equivalent
 */
export interface ServerDownloadChunkSuccessMetadata {
  delay: number;

  /**
   * @alias delAtFirstView
   */
  del_at_first_view: boolean;

  /**
   * @alias queueIndex
   */
  i: number;

  /**
   * @alias keys.server
   */
  id: string;

  name: string;

  part: number;

  size: number;

  /**
   * @alias totalChunks
   */
  total: number;

  type: string;

  zipped: boolean;
}
