export interface UploadedFile {
  serverId: string;
  actionToken: string;
  downloadUrl: string;
}
