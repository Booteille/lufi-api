export interface WebSocketCancelArguments {
  clientKey: string;
}
