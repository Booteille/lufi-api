# lufi-api

## Introduction

Lufi API provides functions needed to interract with Lufi server in your
project. It's built in Typescript and transpiled to Javascript using
[deno](https://deno.com/)


## Table of contents

* [Installation](#installation)
* [Usage](#usage)
  * [Imports](#imports)
  * [Upload Files](#upload-files)
  * [Download a file](#download-a-file)
  * [Pause/Resume a job](#pause-resume-a-job)
  * [Cancel an upload](#cancel-an-upload)
  * [Retrieve informations](#retrieve-informations)
  * [Zipping files](#zipping-files)
  * [Unzipping files](#unzipping-files)


## Installation

You can use [deno](https://deno.com/) to install dependencies:

```bash
deno install
```

Then you can build it for browser by using:

```bash
deno task build
```

Finally, copy content of `dist` folder to your project.

## Usage 

### Imports

You can import lufi in your project using:

```typescript
import { lufi } from "lufi"
```

Since Lufi is using [neverthrow](https://github.com/supermacro/neverthrow), you can either add `neverthrow` as a dependency of your project or use types and functions of `neverthrow` provided by Lufi.

```typescript
import { err, errAsync, ok, okAsync, ResultAsync } from "lufi"
```

### Upload files

You can upload one or multiple files by using the `upload()` function.
The upload is asynchronous and so this function will return a list of upload jobs (one by file).

```typescript
lufi.upload(serverUrl: URL, filesToUpload: File[], delay?: number = 0, delAtFirstView?: boolean, zipped?: boolean, zipName?: string, password?: string, algo?: CryptoAlgorithm
);
```

Example using deno:

```typescript
// Transform Uint8Array files provided by readFileSync() into a File API object
const files = [
  new File([new Blob([Deno.readFileSync("file1.txt")])], "file1.txt"),
  new File([new Blob([Deno.readFileSync("file2.jpg")])], "file2.jpg"),
]

// Run upload jobs and wait for each job completion.
lufi.upload("http://my.local.lufi.server/", files)
.andThen((jobs) => ResultAsync.combine(jobs.map((job) => job.waitForCompletion())))
.orElse((error) => console.error(error))
.then(() => console.debug("Success!"));
```

`waitForJobCompletion()` will wait for the job to terminate before executing the next task.

You can also follow the progress of the upload by using the job function `onProgress()`:

```typescript
// ...

lufi.upload("http://my.local.lufi.server/", files)
.andThen((jobs) => ResultAsync.combine(jobs.map((job) => { 
  job.onProgress(() => {
    console.debug(`Uploaded ${job.lufiFile.chunksReady} / ${job.lufiFile.totalChunks} chunks of ${job.lufiFile.name}`);
  })

  return job.waitForCompletion();
})))
.orElse((error) => console.error(error))
.then(() => console.debug("Success!"));
```

This will print in the console a message each time a chunk is successfuly uploaded to the server.
As you can see, you can access the lufiFile object from the job. This object contains all informations you need about your file.

If you indicate you upload your files with `zipped` set on `true`, Lufi API will automatically compress it in a zip before upload:

```typescript
// ...
lufi.upload(serverUrl, files, undefined, undefined, true, "my-archive.zip")
.andThen((jobs) => job[0].waitForCompletion())
.andThen((job) => {
  console.debug(`File uploaded! Download link: ${job.lufiFile.downloadUrl()}`);

  return okAsync(undefined);
})
.orElse((error) => console.error(error));
```

In this example, there will be only one job in the jobs array, so we don't need to map through it and directly work with the first job.

### Download a file

You can download a file by using the `download()` function.

```typescript
lufi.download(downloadUrl: URL, password?: string);
```

Example:

```typescript
const downloadDir = "./tmp/";
const lufiUrl = new URL("https://you.lufi.server/r/T_9Eaz7zcK#5EY3AekzavcUhHXmUPzgZDRC257LwfOMWzp26MsEVqI=")
const password = "My awesome and very secured password. 101z"

lufi.download(lufiUrl, password).andThen((job) => {
  let downloadedChunks = 0;
  job.onProgress(() => {
    downloadedChunks++;

    console.debug(`Downloaded chunk ${downloadedChunks}/${job.lufiFile.totalChunks}`)
  })

  return job.waitForCompletion();
})
.orElse(async (error) => console.error(error))
.then((job) => {
  // Use Deno to write file to the system.
  if (job.isOk()) {
    const fileBuffer = await job.value.downloadedFile.arrayBuffer();
    Deno.writeFile(downloadDir + job.value.lufiFile.name, new Uint8Array(fileBuffer));
  }
});
```

### Pause/Resume a job

You can pause/resume an upload/download job using `pause()` or `resume()`

Example:

```typescript
lufi.download(downloadUrl)
  .andThen((job) => lufi.pause(job))
  .andThen((job) => {
    console.debug("Job has been paused");

    return okAsync(job);
  })
  .andThen((job) => lufi.resume(job))
  .andThen((job) => lufi.waitForCompletion())
  .andThen((job) => {
    console.debug(`Upload of ${job.lufiFile.name} is now complete!`);

    return okAsync(undefined);
  })
  .orElse((error) => console.error(error));
// ...
```

### Cancel an upload

To ask Lufi server to cancel the upload you can use `cancel()`.

```typescript
// ...
lufi.upload(serverUrl, [file])
.andThen((job) => job.cancel(job))
.andThen((job) => {
  console.debug(`Upload of ${job.lufiFile.name} has been canceled`));

  return okAsync(undefined);
}
.orElse((error) => console.error(error));
```

### Retrieve informations

You can get informations about a file from the Lufi server by using `infos()`:

```typescript
lufi.infos(downloadUrl)
.andThen((job) => {
  console.debug(`The number of days after creation before the file will be deleted is: ${job.lufiFile.delay}`);
  
  return okAsync(undefined);
})
.orElse((error) => console.error(error));
```

### Zipping files

You can manually work with zips using `addFilesToArchive()` and `compress()` functions:

```typescript
// ...
const zipName = "my-archive.zip"

lufi.addFilesToArchive(files)
.andThen((archiveEntries) => lufi.compress(archiveEntries, zipName))
.andThen((zipFile) => {
  console.debug(`Archive size is: ${zipFile.size} `);

  return okAsync(undefined);
})
.orElse((error) => console.error(error));
```

Note that zipping is not working in `deno` runtime for now. `deno` needs to [fix an issue with workers](https://github.com/denoland/deno/issues/26739).

### Unzipping files

You can decompress a Zip archive using `decompress()`.

```typescript
// ...
lufi.decompress(zipFile)
.andThen((files) => {
  files.forEach((file) => {
    console.debug(`Archive contains the file ${file.name}`);
  })

  return okAsync(undefined);
})
.orElse((error) => console.error(error));
```

## Running tests
You can run the test suite by using `deno task test`.

Before running tests, please, think about downloading file-100mb.txt and file-1gb.txt into the test/ folder. We do not provide it to do not occupate a lot of space just for test files. You can use [this website](https://testdatahub.com/generate_files) to generate it.

Note that Zipping tests are skipped until `deno` [fixes an issue](https://github.com/denoland/deno/issues/26739) with `fflate` workers.