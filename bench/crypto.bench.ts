import type { EncryptedData } from "~/interface/encrypted-data.ts";
import * as sjcl from "~/api/crypto/sjcl.ts";
import * as web from "~/api/crypto/web.ts";
import { LufiFile } from "~/entities/lufi-file.ts";
import mime from "mime";
import { CryptoAlgorithm } from "~/index.ts";

let sjclEncrypted: EncryptedData;
let sjclKey: string;

let webEncrypted: EncryptedData;
let webKey: string;

const secretMessage = "I am a secreeeeeeet!";
const secretArrayBuffer = new TextEncoder().encode(secretMessage)
  .buffer as ArrayBuffer;

const file117kbPath = import.meta.dirname + "/../test/file-117kb.jpg";
const file117kb = new File(
  [new Blob([Deno.readFileSync(file117kbPath)])],
  file117kbPath,
  {
    type: mime.getType(file117kbPath) as string,
  },
);
const file117kbBuffer = await file117kb.arrayBuffer();

const file1mbPath = import.meta.dirname + "/../test/file-1mb.jpg";
const file1mb = new File(
  [new Blob([Deno.readFileSync(file1mbPath)])],
  file1mbPath,
  {
    type: mime.getType(file1mbPath) as string,
  },
);
const file1mbBuffer = await file1mb.arrayBuffer();

const file10mbPath = import.meta.dirname + "/../test/file-10mb.mp4";
const file10mb = new File(
  [new Blob([Deno.readFileSync(file10mbPath)])],
  file10mbPath,
  {
    type: mime.getType(file10mbPath) as string,
  },
);
const file10mbBuffer = await file10mb.arrayBuffer();

const worker = new Worker(
  new URL(`../src/worker/encrypt.ts`, new URL(".", import.meta.url).href),
  { type: "module" },
);

Deno.bench(
  "Sjcl.generateKey()",
  { group: "generateKey", baseline: true },
  async () => {
    const result = await sjcl.generateKey();

    if (result.isOk()) {
      sjclKey = result.value;
    }
  },
);

Deno.bench("Web.generateKey()", { group: "generateKey" }, async () => {
  const result = await web.generateKey();

  if (result.isOk()) {
    webKey = result.value;
  }
});

Deno.bench(
  "Sjcl.encrypt(Text)",
  { group: "encrypt text", baseline: true },
  async () => {
    const result = await sjcl.encrypt(sjclKey, secretArrayBuffer);

    if (result.isOk()) {
      sjclEncrypted = result.value;
    }
  },
);
Deno.bench("Web.encrypt(Text)", { group: "encrypt text" }, async () => {
  const result = await web.encrypt(webKey, secretArrayBuffer);

  if (result.isOk()) {
    webEncrypted = result.value;
  }
});

Deno.bench(
  "Sjcl.decrypt(Text)",
  { group: "decrypt text", baseline: true },
  async () => {
    await sjcl.decrypt(sjclKey, sjclEncrypted);
  },
);

Deno.bench("Web.decrypt(Text)", { group: "decrypt text" }, async () => {
  await web.decrypt(webKey, webEncrypted);
});

Deno.bench(
  "Sjcl.encrypt(file117kb)",
  { group: "encrypt file117kb", baseline: true },
  async () => {
    const result = await sjcl.encrypt(sjclKey, file117kbBuffer);

    if (result.isOk()) {
      sjclEncrypted = result.value;
    }
  },
);

Deno.bench(
  "Web.encrypt(file117kb)",
  { group: "encrypt file117kb" },
  async () => {
    const result = await web.encrypt(webKey, file117kbBuffer);

    if (result.isOk()) {
      webEncrypted = result.value;
    }
  },
);

Deno.bench(
  "Sjcl.decrypt(file117kb)",
  { group: "decrypt file117kb", baseline: true },
  async () => {
    await sjcl.decrypt(sjclKey, sjclEncrypted);
  },
);

Deno.bench(
  "Web.decrypt(file117kb)",
  { group: "decrypt file117kb" },
  async () => {
    await web.decrypt(webKey, webEncrypted);
  },
);

Deno.bench("Sjcl.encrypt(file1mb)", {
  group: "encrypt file1mb",
  baseline: true,
}, async () => {
  const result = await sjcl.encrypt(sjclKey, file1mbBuffer);

  if (result.isOk()) {
    sjclEncrypted = result.value;
  }
});

Deno.bench("Web.encrypt(file1mb)", { group: "encrypt file1mb" }, async () => {
  const result = await web.encrypt(webKey, file1mbBuffer);

  if (result.isOk()) {
    webEncrypted = result.value;
  }
});

Deno.bench("Sjcl.decrypt(file1mb)", {
  group: "decrypt file1mb",
  baseline: true,
}, async () => {
  await sjcl.decrypt(sjclKey, sjclEncrypted);
});

Deno.bench("Web.decrypt(file1mb)", { group: "decrypt file1mb" }, async () => {
  await web.decrypt(webKey, webEncrypted);
});

Deno.bench(
  "Sjcl.encrypt(file10mb)",
  { group: "encrypt file10mb", baseline: true },
  async () => {
    const result = await sjcl.encrypt(sjclKey, file10mbBuffer);

    if (result.isOk()) {
      sjclEncrypted = result.value;
    }
  },
);

Deno.bench("Web.encrypt(file10mb)", { group: "encrypt file10mb" }, async () => {
  const result = await web.encrypt(webKey, file10mbBuffer);

  if (result.isOk()) {
    webEncrypted = result.value;
  }
});

Deno.bench(
  "Web Workers: web.encrypt(file10mb)",
  { group: "encrypt file10mb" },
  async () => {
    worker.postMessage({
      args: {
        socketUrl: "http://localhost:7000",
        lufiFile: new LufiFile("http://localhost:7000", {
          keys: { client: webKey, server: "" },
          name: file10mb.name,
          size: file10mb.size,
          type: file10mb.type,
        }),
        chunk: {
          buffer: file10mbBuffer,
          index: 0,
        },
        algo: CryptoAlgorithm.WebCrypto,
      },
    });

    await new Promise((resolve) => {
      worker.onmessage = (event) => {
        webEncrypted = event.data.encryptedData;

        resolve(undefined);
      };
    });
  },
);

Deno.bench(
  "Web Workers: sjcl.encrypt(file10mb)",
  { group: "encrypt file10mb" },
  async () => {
    worker.postMessage({
      args: {
        socketUrl: "http://localhost:7000",
        lufiFile: new LufiFile("http://localhost:7000", {
          keys: { client: sjclKey, server: "" },
          name: file10mb.name,
          size: file10mb.size,
          type: file10mb.type,
        }),
        chunk: {
          buffer: file10mbBuffer,
          index: 0,
        },
        algo: CryptoAlgorithm.Sjcl,
      },
    });

    await new Promise((resolve) => {
      worker.onmessage = (event) => {
        sjclEncrypted = event.data.encryptedData;

        resolve(undefined);
      };
    });
  },
);

Deno.bench(
  "Sjcl.decrypt(file10mb)",
  { group: "decrypt file10mb", baseline: true },
  async () => {
    await sjcl.decrypt(sjclKey, sjclEncrypted);
  },
);

Deno.bench("Web.decrypt(file10mb)", { group: "decrypt file10mb" }, async () => {
  await web.decrypt(webKey, webEncrypted);
});
