import {
  decrypt,
  encrypt,
  generateKey,
  hashPassword,
  isSjclKey,
} from "~/api/crypto/sjcl.ts";
import { generateKey as generateWebKey } from "~/api/crypto/web.ts";
import type { EncryptedData } from "~/interface/encrypted-data.ts";
import { describe, it } from "@std/testing/bdd";
import { expect } from "@std/expect";
import mime from "mime";
import { okAsync } from "~/index.ts";
import { fail } from "@std/assert/fail";

describe("Crypto — Sjcl", () => {
  const secretMessage = "I am a secreeeeeeet!";
  const secretMessageSHA512 =
    "7ef459cf424d51d4afe5c59d92eed27edcf2a4bdc2b6418aefb4b1f60e9a51a02e513a0fb809ad5ae33a9e78cf6646bf81a5ef10e2e37adfa2c4cf6b54659c74";
  const secretArrayBuffer = new TextEncoder().encode(secretMessage)
    .buffer as ArrayBuffer;
  let encryptedString: EncryptedData;
  let key = "";

  const file117kbPath = import.meta.dirname + "/../../file-117kb.jpg";
  const file117kb = new File(
    [new Blob([Deno.readFileSync(file117kbPath)])],
    file117kbPath,
    {
      type: mime.getType(file117kbPath) as string,
    },
  );

  const file1mbPath = import.meta.dirname + "/../../file-1mb.jpg";
  const file1mb = new File(
    [new Blob([Deno.readFileSync(file1mbPath)])],
    file1mbPath,
    {
      type: mime.getType(file1mbPath) as string,
    },
  );

  const file10mbPath = import.meta.dirname + "/../../file-10mb.mp4";
  const file10mb = new File(
    [new Blob([Deno.readFileSync(file10mbPath)])],
    file10mbPath,
    {
      type: mime.getType(file10mbPath) as string,
    },
  );

  let encrypted117kb: EncryptedData;
  let encrypted1mb: EncryptedData;
  let encrypted10mb: EncryptedData;

  describe("When generating a key", () => {
    it("Should generate a random base64url encoded string", async () => {
      (await generateKey()).match(
        (str) => {
          key = str;

          expect(typeof key).toBe("string");
        },
        (err) => {
          throw err;
        },
      );
    });

    it("Should have length equals to 44", () => {
      expect(key).toHaveLength(44);
    });
  });

  describe("When hashing a string", () => {
    it("Should successfully hash a string using sha512", async () => {
      await hashPassword(secretMessage).andThen((hashedPassword) => {
        expect(hashedPassword).toBe(secretMessageSHA512);

        return okAsync(undefined);
      }).orElse((error) => fail(error.message));
    });
  });

  describe("When encrypting", () => {
    it("Should encrypt a string into a SjclCipherEncrypted", async () => {
      (await encrypt(key, secretArrayBuffer)).match(
        (encryptedData) => {
          encryptedString = encryptedData;

          const entries = Object.entries(
            JSON.parse(
              new TextDecoder().decode(encryptedString.data as ArrayBuffer),
            ),
          );

          for (const [key] of entries) {
            expect([
              "adata",
              "cipher",
              "ct",
              "iter",
              "iv",
              "ks",
              "mode",
              "salt",
              "ts",
              "v",
            ]).toContain(key);
          }

          expect(entries.length).toBe(10);
        },
        (err) => {
          throw err;
        },
      );
    });

    it("Should encrypt a 117kb file", async () => {
      (await encrypt(key, await file117kb.arrayBuffer())).match(
        (encryptedData) => {
          encrypted117kb = encryptedData;

          const entries = Object.entries(
            JSON.parse(
              new TextDecoder().decode(encryptedData.data as ArrayBuffer),
            ),
          );

          for (const [key] of entries) {
            expect([
              "adata",
              "cipher",
              "ct",
              "iter",
              "iv",
              "ks",
              "mode",
              "salt",
              "ts",
              "v",
            ]).toContain(key);
          }

          expect(entries.length).toBe(10);

          expect(typeof encrypted117kb.iv).toBe("string");
          expect(encrypted117kb.iv).not.toBe("");
        },
        (err) => {
          throw err;
        },
      );
    });

    it("Should encrypt a 1mb file", async () => {
      (await encrypt(key, await file1mb.arrayBuffer())).match(
        (encryptedData) => {
          encrypted1mb = encryptedData;

          const entries = Object.entries(
            JSON.parse(
              new TextDecoder().decode(encryptedData.data as ArrayBuffer),
            ),
          );

          for (const [key] of entries) {
            expect([
              "adata",
              "cipher",
              "ct",
              "iter",
              "iv",
              "ks",
              "mode",
              "salt",
              "ts",
              "v",
            ]).toContain(key);
          }

          expect(entries.length).toBe(10);

          expect(typeof encrypted1mb.iv).toBe("string");
          expect(encrypted1mb.iv).not.toBe("");
        },
        (err) => {
          throw err;
        },
      );
    });

    it("Should encrypt a 10mb file", async () => {
      (await encrypt(key, await file10mb.arrayBuffer())).match(
        (encryptedData) => {
          encrypted10mb = encryptedData;

          const entries = Object.entries(
            JSON.parse(
              new TextDecoder().decode(encryptedData.data as ArrayBuffer),
            ),
          );

          for (const [key] of entries) {
            expect([
              "adata",
              "cipher",
              "ct",
              "iter",
              "iv",
              "ks",
              "mode",
              "salt",
              "ts",
              "v",
            ]).toContain(key);
          }

          expect(entries.length).toBe(10);

          expect(typeof encrypted10mb.iv).toBe("string");
          expect(encrypted10mb.iv).not.toBe("");
        },
        (err) => {
          throw err;
        },
      );
    });
  });

  describe("When decrypting", () => {
    it("Should decrypt a string", async () => {
      (await decrypt(key, encryptedString)).match(
        (str) => expect(new TextDecoder().decode(str)).toBe(secretMessage),
        (err) => {
          throw err;
        },
      );
    });
    it("Should decrypt a 117kb file", async () => {
      (await decrypt(key, encrypted117kb)).match(
        async (decryptedFile) => {
          expect(decryptedFile).toEqual(await file117kb.arrayBuffer());
        },
        (err) => {
          throw err;
        },
      );
    });
    it("Should decrypt a 1mb file", async () => {
      (await decrypt(key, encrypted1mb)).match(
        async (decryptedFile) => {
          expect(decryptedFile).toEqual(await file1mb.arrayBuffer());
        },
        (err) => {
          throw err;
        },
      );
    });

    it("Should decrypt a 10mb file", async () => {
      return (await decrypt(key, encrypted10mb)).match(
        async (decryptedFile) => {
          expect(decryptedFile).toEqual(await file10mb.arrayBuffer());
        },
        (err) => {
          throw err;
        },
      );
    });
  });

  describe("When detecting a key", () => {
    it("Should be able to know if it's a Sjcl key", async () => {
      const sjclKey = (await generateKey()).unwrapOr("");
      const webKey = (await generateWebKey()).unwrapOr("");

      expect(isSjclKey(sjclKey)).toBeTruthy();
      expect(isSjclKey(webKey)).toBeFalsy();
    });
  });
});
