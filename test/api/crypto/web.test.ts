import { describe, it } from "@std/testing/bdd";
import { expect } from "@std/expect";
import {
  decrypt,
  encrypt,
  generateKey,
  hashPassword,
  importKey,
} from "~/api/crypto/web.ts";
import type { EncryptedData } from "~/interface/encrypted-data.ts";
import mime from "mime";
import { okAsync } from "neverthrow";
import { fail } from "@std/assert/fail";

describe("Crypto — WebCrypto", () => {
  const secretMessage = "I am a secreeeeeeet!";
  const secretMessageSHA512 =
    "7ef459cf424d51d4afe5c59d92eed27edcf2a4bdc2b6418aefb4b1f60e9a51a02e513a0fb809ad5ae33a9e78cf6646bf81a5ef10e2e37adfa2c4cf6b54659c74";
  let encryptedMessage: EncryptedData;
  let randomString = "";
  let key: CryptoKey;

  const file117kbPath = import.meta.dirname + "/../../file-117kb.jpg";
  const file117kb = new File(
    [new Blob([Deno.readFileSync(file117kbPath)])],
    file117kbPath,
    {
      type: mime.getType(file117kbPath) as string,
    },
  );

  const file1mbPath = import.meta.dirname + "/../../file-1mb.jpg";
  const file1mb = new File(
    [new Blob([Deno.readFileSync(file1mbPath)])],
    file1mbPath,
    {
      type: mime.getType(file1mbPath) as string,
    },
  );

  const file10mbPath = import.meta.dirname + "/../../file-10mb.mp4";
  const file10mb = new File(
    [new Blob([Deno.readFileSync(file10mbPath)])],
    file10mbPath,
    {
      type: mime.getType(file10mbPath) as string,
    },
  );

  let encrypted117kb: EncryptedData;
  let encrypted1mb: EncryptedData;
  let encrypted10mb: EncryptedData;

  describe("When generating a random string", () => {
    it("Should generate a random string", async () => {
      (await generateKey()).match(
        (str) => {
          randomString = str;
          expect(typeof randomString).toBe("string");
        },
        (err) => {
          throw err;
        },
      );
    });

    it("Should have length equals to 43", () => {
      expect(randomString).toHaveLength(43);
    });
  });

  describe("When generating a key", () => {
    it("Should generate a CryptoKey from random string", async () => {
      (await importKey(randomString)).match(
        (str) => {
          key = str;
          expect(key).toBeInstanceOf(CryptoKey);
        },
        (err) => {
          throw err;
        },
      );
    });
  });

  describe("When hashing a string", () => {
    it("Should successfully hash a string using sha512", async () => {
      await hashPassword(secretMessage).andThen((hashedPassword) => {
        expect(hashedPassword).toBe(secretMessageSHA512);

        return okAsync(undefined);
      }).orElse((error) => fail(error.message));
    });
  });

  describe("When encrypting", () => {
    it("Should encrypt a string", async () => {
      (
        await encrypt(
          randomString,
          new TextEncoder().encode(secretMessage).buffer as ArrayBuffer,
        )
      ).match(
        (encryptedData) => {
          encryptedMessage = encryptedData;
          expect(encryptedMessage.data).toBeInstanceOf(ArrayBuffer);
          expect(encryptedMessage.iv).toBeInstanceOf(Uint8Array);
        },
        (err) => {
          throw err;
        },
      );
    });

    it("Should encrypt a 117kb file", async () => {
      (await encrypt(randomString, await file117kb.arrayBuffer())).match(
        (encryptedData) => {
          encrypted117kb = encryptedData;

          expect(encrypted117kb.data).toBeInstanceOf(ArrayBuffer);
          expect(encrypted117kb.iv).toBeInstanceOf(Uint8Array);
        },
        (err) => {
          throw err;
        },
      );
    });

    it("Should encrypt a 1mb file", async () => {
      (await encrypt(randomString, await file1mb.arrayBuffer())).match(
        (encryptedData) => {
          encrypted1mb = encryptedData;

          expect(encrypted1mb.data).toBeInstanceOf(ArrayBuffer);
          expect(encrypted1mb.iv).toBeInstanceOf(Uint8Array);
        },
        (err) => {
          throw err;
        },
      );
    });

    it("Should encrypt a 10mb file", async () => {
      (await encrypt(randomString, await file10mb.arrayBuffer())).match(
        (encryptedData) => {
          encrypted10mb = encryptedData;

          expect(encrypted10mb.data).toBeInstanceOf(ArrayBuffer);
          expect(encrypted10mb.iv).toBeInstanceOf(Uint8Array);
        },
        (err) => {
          throw err;
        },
      );
    });
  });

  describe("When decrypting", () => {
    it("Should decrypt a string", async () => {
      (await decrypt(randomString, encryptedMessage)).match(
        (decryptedData) => {
          expect(new TextDecoder().decode(decryptedData)).toBe(secretMessage);
        },
        (err) => {
          throw err;
        },
      );
    });

    it("Should decrypt a 117KB file", async () => {
      (await decrypt(randomString, encrypted117kb)).match(
        async (decryptedData) => {
          expect(decryptedData.byteLength).toBe(
            (await file117kb.arrayBuffer()).byteLength,
          );
        },
        (err) => {
          throw err;
        },
      );
    });

    it("Should decrypt a 1MB file", async () => {
      (await decrypt(randomString, encrypted1mb)).match(
        async (decryptedData) => {
          expect(decryptedData.byteLength).toBe(
            (await file1mb.arrayBuffer()).byteLength,
          );
        },
        (err) => {
          throw err;
        },
      );
    });

    it("Should decrypt a 10MB file", async () => {
      (await decrypt(randomString, encrypted10mb)).match(
        async (decryptedData) => {
          expect(decryptedData.byteLength).toBe(
            (await file10mb.arrayBuffer()).byteLength,
          );
        },
        (err) => {
          throw err;
        },
      );
    });
  });
});
