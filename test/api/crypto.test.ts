import { describe, it } from "@std/testing/bdd";
import { expect } from "@std/expect";
import mime from "mime";

import { decrypt, encrypt, generateKey, hashPassword } from "~/api/crypto.ts";
import { isSjclKey } from "~/api/crypto/sjcl.ts";
import { CryptoAlgorithm } from "~/enum/crypto-algorithm.ts";
import type { EncryptedData } from "~/interface/encrypted-data.ts";

import { okAsync } from "neverthrow";
import { fail } from "@std/assert/fail";

describe("Crypto", () => {
  const secretMessage = "I am a secreeeeeeet!";
  const secretMessageSHA512 =
    "7ef459cf424d51d4afe5c59d92eed27edcf2a4bdc2b6418aefb4b1f60e9a51a02e513a0fb809ad5ae33a9e78cf6646bf81a5ef10e2e37adfa2c4cf6b54659c74";
  const secretArrayBuffer = new TextEncoder().encode(secretMessage)
    .buffer as ArrayBuffer;
  let webEncrypted: EncryptedData;
  let sjclEncrypted: EncryptedData;
  let webKey: string;
  let sjclKey: string;

  const file117kbPath = import.meta.dirname + "/../file-117kb.jpg";
  const file117kb = new File(
    [new Blob([Deno.readFileSync(file117kbPath)])],
    file117kbPath,
    {
      type: mime.getType(file117kbPath) as string,
    },
  );

  const file1mbPath = import.meta.dirname + "/../file-1mb.jpg";
  const file1mb = new File(
    [new Blob([Deno.readFileSync(file1mbPath)])],
    file1mbPath,
    {
      type: mime.getType(file1mbPath) as string,
    },
  );

  const file10mbPath = import.meta.dirname + "/../file-10mb.mp4";
  const file10mb = new File(
    [new Blob([Deno.readFileSync(file10mbPath)])],
    file10mbPath,
    {
      type: mime.getType(file10mbPath) as string,
    },
  );

  let webEncryptedFile117kb: EncryptedData;
  let sjclEncryptedFile117kb: EncryptedData;
  let webEncryptedFile1mb: EncryptedData;
  let sjclEncryptedFile1mb: EncryptedData;
  let webEncryptedFile10mb: EncryptedData;
  let sjclEncryptedFile10mb: EncryptedData;

  describe("When generating a key", () => {
    it("Should generate a WebCrypto key by default", async () => {
      (await generateKey()).match(
        (key) => {
          webKey = key;

          expect(typeof webKey).toBe("string");
          expect(isSjclKey(webKey)).toBeFalsy();
        },
        (err) => {
          throw err;
        },
      );
    });

    it("Should be able to generate a Sjcl key", async () => {
      (await generateKey(CryptoAlgorithm.Sjcl)).match(
        (key) => {
          sjclKey = key;

          expect(typeof sjclKey).toBe("string");
          expect(isSjclKey(sjclKey)).toBeTruthy();
        },
        (err) => {
          throw err;
        },
      );
    });
  });

  describe("When hashing a string", () => {
    it("Should successfully hash a string using WebCrypto API", async () => {
      await hashPassword(secretMessage, CryptoAlgorithm.WebCrypto).andThen(
        (hashedPassword) => {
          expect(hashedPassword).toBe(secretMessageSHA512);

          return okAsync(undefined);
        },
      ).orElse((error) => fail(error.message));
    });

    it("Should successfully hash a string using Sjcl API", async () => {
      await hashPassword(secretMessage, CryptoAlgorithm.Sjcl).andThen(
        (hashedPassword) => {
          expect(hashedPassword).toBe(secretMessageSHA512);

          return okAsync(undefined);
        },
      ).orElse((error) => fail(error.message));
    });
  });

  describe("String", () => {
    describe("When encrypting a string", () => {
      it("Should encrypt using WebCrypto API", async () => {
        (await encrypt(webKey, secretArrayBuffer, CryptoAlgorithm.WebCrypto))
          .match(
            (encryptedData) => {
              webEncrypted = encryptedData;

              expect(webEncrypted.data).toBeInstanceOf(ArrayBuffer);
              expect(webEncrypted.iv).toBeInstanceOf(Uint8Array);
            },
            (err) => {
              throw err;
            },
          );
      });

      it("Should be able to encrypt using Sjcl API", async () => {
        (await encrypt(sjclKey, secretArrayBuffer, CryptoAlgorithm.Sjcl)).match(
          (encryptedData) => {
            sjclEncrypted = encryptedData;

            const entries = Object.entries(
              JSON.parse(
                new TextDecoder().decode(encryptedData.data as ArrayBuffer),
              ),
            );

            for (const [key] of entries) {
              expect([
                "adata",
                "cipher",
                "ct",
                "iter",
                "iv",
                "ks",
                "mode",
                "salt",
                "ts",
                "v",
              ]).toContain(key);
            }

            expect(entries.length).toBe(10);
          },
          (err) => {
            throw err;
          },
        );
      });
    });

    describe("When decrypting a string", () => {
      it("Should automaticaly detect the default decryption algorithm to use", async () => {
        let webDecrypted = "";
        (await decrypt(webKey, webEncrypted)).match(
          (encryptedData) => {
            webDecrypted = new TextDecoder().decode(
              encryptedData,
            );
          },
          (err) => {
            throw err;
          },
        );
        let sjclDecrypted = "";

        (await decrypt(sjclKey, sjclEncrypted)).match(
          (encryptedData) => {
            sjclDecrypted = new TextDecoder().decode(
              encryptedData,
            );
          },
          (err) => {
            throw err;
          },
        );

        expect(webDecrypted).toBe(secretMessage);
        expect(sjclDecrypted).toBe(secretMessage);
      });

      it("Should be able to decrypt a string encrypted with WebCrypto API", async () => {
        (await decrypt(webKey, webEncrypted)).match(
          (decryptedData) => {
            expect(new TextDecoder().decode(decryptedData)).toBe(
              secretMessage,
            );
          },
          (err) => {
            throw err;
          },
        );
      });

      it("Should be able to decrypt a string encrypted with Sjcl API", async () => {
        (await decrypt(sjclKey, sjclEncrypted)).match(
          (decryptedData) => {
            expect(new TextDecoder().decode(decryptedData as ArrayBuffer)).toBe(
              secretMessage,
            );
          },
          (err) => {
            throw err;
          },
        );
      });
    });
  });

  describe("117kb file", () => {
    describe("encrypt", () => {
      it("Should encrypt a file using WebCrypto", async () => {
        (await encrypt(
          webKey,
          await file117kb.arrayBuffer(),
          CryptoAlgorithm.WebCrypto,
        )).match(
          (encryptedData) => {
            webEncryptedFile117kb = encryptedData;
            expect(webEncryptedFile117kb.data).toBeInstanceOf(
              ArrayBuffer,
            );
            expect(webEncryptedFile117kb.iv).toBeInstanceOf(Uint8Array);
          },
          (err) => {
            throw err;
          },
        );
      });

      it("Should encrypt a file using Sjcl", async () => {
        (await encrypt(
          sjclKey,
          await file117kb.arrayBuffer(),
          CryptoAlgorithm.Sjcl,
        )).match(
          (encryptedData) => {
            sjclEncryptedFile117kb = encryptedData;

            const entries = Object.entries(
              JSON.parse(
                new TextDecoder().decode(encryptedData.data as ArrayBuffer),
              ),
            );

            for (const [key] of entries) {
              expect([
                "adata",
                "cipher",
                "ct",
                "iter",
                "iv",
                "ks",
                "mode",
                "salt",
                "ts",
                "v",
              ]).toContain(key);
            }

            expect(entries.length).toBe(10);

            expect(sjclEncryptedFile117kb.data).toBeInstanceOf(
              ArrayBuffer,
            );
            expect(sjclEncryptedFile117kb.iv).not.toBe("");
          },
          (err) => {
            throw err;
          },
        );
      });
    });

    describe("Decrypt", () => {
      it("Should decrypt a file encrypted with WebCrypto API", async () => {
        (await decrypt(webKey, webEncryptedFile117kb)).match(
          async (decryptedFile) => {
            expect(decryptedFile).toEqual(await file117kb.arrayBuffer());
          },
          (err) => {
            throw err;
          },
        );
      });

      it("Should decrypt a file encrypted with WebCrypto API", async () => {
        (await decrypt(sjclKey, sjclEncryptedFile117kb)).match(
          async (decryptedFile) => {
            expect(decryptedFile).toEqual(await file117kb.arrayBuffer());
          },
          (err) => {
            throw err;
          },
        );
      });
    });
  });

  describe("1mb file", () => {
    describe("encrypt", () => {
      it("Should encrypt a file using WebCrypto", async () => {
        (await encrypt(
          webKey,
          await file1mb.arrayBuffer(),
          CryptoAlgorithm.WebCrypto,
        )).match(
          (encryptedData) => {
            webEncryptedFile1mb = encryptedData;
            expect(webEncryptedFile1mb.data).toBeInstanceOf(ArrayBuffer);
            expect(webEncryptedFile1mb.iv).toBeInstanceOf(Uint8Array);
          },
          (err) => {
            throw err;
          },
        );
      });

      it("Should encrypt a file using Sjcl", async () => {
        (await encrypt(
          sjclKey,
          await file1mb.arrayBuffer(),
          CryptoAlgorithm.Sjcl,
        )).match(
          (encryptedData) => {
            sjclEncryptedFile1mb = encryptedData;

            const entries = Object.entries(
              JSON.parse(
                new TextDecoder().decode(encryptedData.data as ArrayBuffer),
              ),
            );

            for (const [key] of entries) {
              expect([
                "adata",
                "cipher",
                "ct",
                "iter",
                "iv",
                "ks",
                "mode",
                "salt",
                "ts",
                "v",
              ]).toContain(key);
            }

            expect(entries.length).toBe(10);

            expect(sjclEncryptedFile1mb.data).toBeInstanceOf(
              ArrayBuffer,
            );
            expect(sjclEncryptedFile1mb.iv).not.toBe("");
          },
          (err) => {
            throw err;
          },
        );
      });
    });

    describe("Decrypt", () => {
      it("Should decrypt a file encrypted with WebCrypto API", async () => {
        (await decrypt(webKey, webEncryptedFile1mb)).match(
          async (decryptedFile) => {
            expect(decryptedFile).toEqual(await file1mb.arrayBuffer());
          },
          (err) => {
            throw err;
          },
        );
      });

      it("Should decrypt a file encrypted with WebCrypto API", async () => {
        (await decrypt(sjclKey, sjclEncryptedFile1mb)).match(
          async (decryptedFile) => {
            expect(decryptedFile).toEqual(await file1mb.arrayBuffer());
          },
          (err) => {
            throw err;
          },
        );
      });
    });
  });

  describe("10mb file", () => {
    describe("encrypt", () => {
      it("Should encrypt a file using WebCrypto", async () => {
        (await encrypt(
          webKey,
          await file10mb.arrayBuffer(),
          CryptoAlgorithm.WebCrypto,
        )).match(
          (encryptedData) => {
            webEncryptedFile10mb = encryptedData;

            expect(webEncryptedFile10mb.data).toBeInstanceOf(
              ArrayBuffer,
            );
            expect(webEncryptedFile10mb.iv).toBeInstanceOf(Uint8Array);
          },
          (err) => {
            throw err;
          },
        );
      });

      it("Should encrypt a file using Sjcl", async () => {
        (await encrypt(
          sjclKey,
          await file10mb.arrayBuffer(),
          CryptoAlgorithm.Sjcl,
        )).match(
          (encryptedData) => {
            sjclEncryptedFile10mb = encryptedData;

            const entries = Object.entries(
              JSON.parse(
                new TextDecoder().decode(encryptedData.data as ArrayBuffer),
              ),
            );

            for (const [key] of entries) {
              expect([
                "adata",
                "cipher",
                "ct",
                "iter",
                "iv",
                "ks",
                "mode",
                "salt",
                "ts",
                "v",
              ]).toContain(key);
            }

            expect(entries.length).toBe(10);

            expect(sjclEncryptedFile10mb.data).toBeInstanceOf(
              ArrayBuffer,
            );
            expect(sjclEncryptedFile10mb.iv).not.toBe("");
          },
          (err) => {
            throw err;
          },
        );
      });
    });

    describe("Decrypt", () => {
      it("Should decrypt a file encrypted with WebCrypto API", async () => {
        (await decrypt(webKey, webEncryptedFile10mb)).match(
          async (decryptedFile) => {
            expect(decryptedFile).toEqual(await file10mb.arrayBuffer());
          },
          (err) => {
            throw err;
          },
        );
      });

      it("Should decrypt a file encrypted with WebCrypto API", async () => {
        (await decrypt(sjclKey, sjclEncryptedFile10mb)).match(
          async (decryptedFile) => {
            expect(decryptedFile).toEqual(await file10mb.arrayBuffer());
          },
          (err) => {
            throw err;
          },
        );
      });
    });
  });
});
