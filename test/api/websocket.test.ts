import * as ws from "~/api/websocket.ts";
import { encrypt, generateKey } from "~/api/crypto.ts";
import { describe, it } from "@std/testing/bdd";
import { expect } from "@std/expect";
import { fail } from "@std/assert";
import mime from "mime";
import { LufiFile } from "~/entities/lufi-file.ts";
import { okAsync } from "neverthrow";
import { CryptoAlgorithm } from "~/enum/crypto-algorithm.ts";

// Must have lufi open to test it
const serverUrl = "http://127.0.0.1:8081";
const WS_URL = new URL("ws://127.0.0.1:8081/upload");
const socketKey = WS_URL.toString();

const file117kbPath = import.meta.dirname + "/../file-117kb.jpg";
const file117kb = new File(
  [new Blob([Deno.readFileSync(file117kbPath)])],
  file117kbPath,
  {
    type: mime.getType(file117kbPath) as string,
  },
);

describe("Websocket", () => {
  it("should spawn a websocket", async () => {
    await ws
      .spawn(socketKey)
      .map((socketKey) => {
        expect(ws.sockets[socketKey]).toBeInstanceOf(WebSocket);
      })
      .mapErr((error) => fail(error.message));
  });

  it("should have an open websocket", () => {
    expect(ws.isSpawned(socketKey)).toBeTruthy();
  });

  it("should send a chunk", async () => {
    const fileArrayBuffer = await file117kb.arrayBuffer();
    let lufiFile: LufiFile;

    if (file117kb.name) {
      await generateKey()
        .andThen((clientKey) => {
          lufiFile = new LufiFile(serverUrl, {
            name: file117kb.name,
            size: file117kb.size,
            type: file117kb.type,
            keys: { client: clientKey, server: "" },
          });

          return okAsync(lufiFile);
        })
        .andThen((lufiFile) =>
          encrypt(
            lufiFile.keys.client,
            fileArrayBuffer,
            CryptoAlgorithm.WebCrypto,
          )
        )
        .andThen((encryptedFile) =>
          ws.uploadChunk(
            lufiFile,
            {
              total: 1,
              part: 0,
              size: file117kb.size,
              name: file117kb.name as string,
              file_pwd: "",
              type: file117kb.type,
              delay: 0,
              del_at_first_view: false,
              zipped: false,
              id: null,
              i: 0,
            },
            encryptedFile,
          )
        );
    } else {
      fail("File name unavailable");
    }
  });

  it("should close an open websocket", async () => {
    await ws.close(socketKey).mapErr((error) => fail(error.message));
  });
});
