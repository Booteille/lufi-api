import { describe, it } from "@std/testing/bdd";
import { expect } from "@std/expect";
import { fail } from "@std/assert";
import { ensureError, fetchServerConfig, isSecureContext } from "~/utils.ts";
import { ConnectionError } from "~/error/connection-error.ts";

describe("In Utils", () => {
  describe("While trying to ensure errors", () => {
    describe("It should return an Error object", () => {
      it("When providing an Error object", () => {
        const error = ensureError(new Error("lol"));

        expect(error).toBeInstanceOf(Error);
      });

      it("When providing an ConnectionError object", () => {
        const error = ensureError(new ConnectionError());

        expect(error).toBeInstanceOf(Error);
        expect(error).toBeInstanceOf(ConnectionError);
      });

      it("When providing any other object", () => {
        const error = ensureError(new Object());

        expect(error).toBeInstanceOf(Error);
      });

      it("When providing a string", () => {
        const error = ensureError("error");

        expect(error).toBeInstanceOf(Error);
      });
    });
  });

  describe("When retrieving Lufi config", () => {
    it("Should succeed to retrieve the config of a Lufi Server", async () => {
      await fetchServerConfig(new URL("http://127.0.0.1:8081/")).match(
        (config) => {
          expect(typeof config).toBe("object");
          expect(config).toHaveProperty("allow_pwd_on_files");
          expect(config).toHaveProperty("broadcast_message");
          expect(config).toHaveProperty("default_delay");
          expect(config).toHaveProperty("delay_for_size");
          expect(config).toHaveProperty("force_burn_after_reading");
          expect(config).toHaveProperty("instance_name");
          expect(config).toHaveProperty("keep_ip_during");
          expect(config).toHaveProperty("max_delay");
          expect(config).toHaveProperty("max_file_size");
          expect(config).toHaveProperty("need_authentication");
          expect(config).toHaveProperty("report");
          expect(config).toHaveProperty("stop_upload");
          expect(config).toHaveProperty("version");
        },
        (err) => fail(err.message),
      );
    });

    it("Should fail to retrieve the config from a non-existant Lufi Server", async () => {
      const result = fetchServerConfig(
        new URL("http://does-not-exist-ca68z4.com:8081/"),
      );

      expect((await result).isErr()).toBeTruthy();
    });
  });

  it("Should define deno as a secure context", () => {
    expect(isSecureContext()).toBeTruthy();
  });
});
