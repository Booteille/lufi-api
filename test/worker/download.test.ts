import { beforeAll, describe, it } from "@std/testing/bdd";
import { expect } from "@std/expect";
import mime from "mime";
import { fail } from "@std/assert";
import { okAsync, ResultAsync } from "neverthrow";
import * as Lufi from "~/api/lufi.ts";
import type { LufiFile } from "~/entities/lufi-file.ts";
import { ensureError } from "~/utils.ts";
import { JobStatus } from "~/enum/job-status.ts";
import ProgressBar from "@deno-library/progress";

const serverUrl = new URL("http://127.0.0.1:8081/");

const file10mbPath = import.meta.dirname + "/../file-10mb.mp4";
const file10mb = new File(
  [new Blob([Deno.readFileSync(file10mbPath)])],
  file10mbPath,
  {
    type: mime.getType(file10mbPath) as string,
  },
);

const password = "mdp";

const downloadDir = import.meta.dirname + "/../../tmp/";

const externalUrl = new URL(
  "https://asso.framasoft.org/drop/r/uDvKt1bLTp#t7As+ojgCxSrEYrlu9+PSAL3j+GH95VZ2aX1hPIWypY=",
);
const externalUrlWithPassword = new URL(
  "https://asso.framasoft.org/drop/r/Xv01n9yWML#MrAAe/ARA9+R+6xEQ32G6/Vzw0Gazk06whJegR+oDyU=",
);

const openAsFile = (downloadPath: string) =>
  new File([Deno.readFileSync(downloadPath)], downloadPath, {
    type: mime.getType(file10mbPath) as string,
  });

let localUpload: LufiFile;
let localUploadWithPassword: LufiFile;

describe("Download", () => {
  beforeAll(async () => {
    await Lufi.upload(serverUrl, [file10mb])
      .andThen((jobs) => jobs[0].waitForCompletion())
      .map((job) => {
        localUpload = job.lufiFile;
      });

    await Lufi.upload(
      serverUrl,
      [file10mb],
      undefined,
      undefined,
      undefined,
      undefined,
      password,
    )
      .andThen((jobs) => jobs[0].waitForCompletion())
      .map((job) => {
        localUploadWithPassword = job.lufiFile;
      });
  });

  describe("Without password", () => {
    it("Should upload then download a file to local server", async () => {
      let lufiFile: LufiFile;
      let downloadPath: string;

      await Lufi.download(localUpload.downloadUrl())
        .andThen((job) => job.waitForCompletion())
        .andThen((job) => {
          if (job.downloadedFile) {
            lufiFile = job.lufiFile;
            downloadPath = downloadDir + job.lufiFile.name;

            return ResultAsync.fromPromise(
              job.downloadedFile.arrayBuffer(),
              (error) => ensureError(error),
            ).andThen((arrayBuffer) =>
              ResultAsync.fromPromise(
                Deno.writeFile(downloadPath, new Uint8Array(arrayBuffer)),
                (error) => ensureError(error),
              )
            );
          } else {
            fail("Pas de fichier temporaire");
          }
        })
        .andThen(() => {
          const downloadedFile = openAsFile(downloadPath);

          expect(downloadedFile.size).toBe(lufiFile.size);
          expect(downloadedFile.type).toBe(lufiFile.type);
          expect(downloadedFile.name?.split("/").pop()).toBe(lufiFile.name);

          return okAsync(undefined);
        })
        .orElse((error) => fail(error.message));
    });

    it("Should download a file from an external server", async () => {
      let lufiFile: LufiFile;
      let downloadPath: string;
      let progress: ProgressBar;

      await Lufi.download(externalUrl)
        .andThen((job) => job.waitForStart())
        .andThen((job) => {
          progress = new ProgressBar({
            title: `Downloading ${job.lufiFile.name}`,
            total: job.lufiFile.totalChunks,
          });

          job.onProgress(() => {
            progress.render(job.lufiFile.chunksReady);
          });

          return job.waitForCompletion();
        })
        .andThen((job) => {
          if (job.downloadedFile) {
            lufiFile = job.lufiFile;
            downloadPath = downloadDir + job.lufiFile.name;

            return ResultAsync.fromPromise(
              job.downloadedFile.arrayBuffer(),
              (error) => ensureError(error),
            ).andThen((arrayBuffer) =>
              ResultAsync.fromPromise(
                Deno.writeFile(downloadPath, new Uint8Array(arrayBuffer)),
                (error) => ensureError(error),
              )
            );
          } else {
            fail("Pas de fichier temporaire");
          }
        })
        .andThen(() => {
          const downloadedFile = openAsFile(downloadPath);

          expect(downloadedFile.size).toBe(lufiFile.size);
          expect(downloadedFile.type).toBe(lufiFile.type);
          expect(downloadedFile.name?.split("/").pop()).toBe(lufiFile.name);

          return okAsync(undefined);
        })
        .orElse((error) => fail(error.message));
    });
  });

  describe("With password", () => {
    it("Should upload then download a file to local server", async () => {
      let lufiFile: LufiFile;
      let downloadPath: string;

      await Lufi.download(localUploadWithPassword.downloadUrl(), password)
        .andThen((job) => job.waitForCompletion())
        .andThen((job) => {
          if (job.downloadedFile) {
            lufiFile = job.lufiFile;
            downloadPath = downloadDir + job.lufiFile.name;

            return ResultAsync.fromPromise(
              job.downloadedFile.arrayBuffer(),
              (error) => ensureError(error),
            ).andThen((arrayBuffer) =>
              ResultAsync.fromPromise(
                Deno.writeFile(downloadPath, new Uint8Array(arrayBuffer)),
                (error) => ensureError(error),
              )
            );
          } else {
            fail("Temporary file is missing");
          }
        })
        .andThen(() => {
          const downloadedFile = openAsFile(downloadPath);

          expect(downloadedFile.size).toBe(lufiFile.size);
          expect(downloadedFile.type).toBe(lufiFile.type);
          expect(downloadedFile.name?.split("/").pop()).toBe(lufiFile.name);

          return okAsync(undefined);
        })
        .orElse((error) => fail(error.message));
    });

    it("Should download a file from an external server", async () => {
      let lufiFile: LufiFile;
      let downloadPath: string;
      let progress: ProgressBar;

      await Lufi.download(externalUrlWithPassword, password)
        .andThen((job) => job.waitForStart())
        .andThen((job) => {
          progress = new ProgressBar({
            title: `Downloading ${job.lufiFile.name}`,
            total: job.lufiFile.totalChunks,
          });

          job.onProgress(() => {
            progress.render(job.lufiFile.chunksReady);
          });

          return job.waitForCompletion();
        })
        .andThen((job) => {
          if (job.downloadedFile) {
            lufiFile = job.lufiFile;
            downloadPath = downloadDir + job.lufiFile.name;

            return ResultAsync.fromPromise(
              job.downloadedFile.arrayBuffer(),
              (error) => ensureError(error),
            ).andThen((arrayBuffer) =>
              ResultAsync.fromPromise(
                Deno.writeFile(downloadPath, new Uint8Array(arrayBuffer)),
                (error) => ensureError(error),
              )
            );
          } else {
            fail("Pas de fichier temporaire");
          }
        })
        .andThen(() => {
          const downloadedFile = openAsFile(downloadPath);

          expect(downloadedFile.size).toBe(lufiFile.size);
          expect(downloadedFile.type).toBe(lufiFile.type);
          expect(downloadedFile.name?.split("/").pop()).toBe(lufiFile.name);

          return okAsync(undefined);
        })
        .orElse((error) => fail(error.message));
    });
  });

  describe("With wrong parameters", () => {
    describe("Without its password", () => {
      it("Should fail to download an localy uploaded file ", () => {
        Lufi.download(localUploadWithPassword.downloadUrl())
          .andThen((job) => job.waitForCompletion())
          .andThen(() => {
            fail("Should not be able to download this file");
          })
          .orElse((error) => {
            expect(error).toBeInstanceOf(Error);
            return okAsync(undefined);
          });
      });

      it("Should fail to download an external file ", () => {
        Lufi.download(externalUrlWithPassword)
          .andThen((job) => job.waitForCompletion())
          .andThen(() => {
            fail("Should not be able to download this file");
          })
          .orElse((error) => {
            expect(error).toBeInstanceOf(Error);
            return okAsync(undefined);
          });
      });
    });
  });

  describe("onProgress", () => {
    it("Should be able to use a callback", async () => {
      let i = 0;

      await Lufi.download(localUpload.downloadUrl())
        .andThen((job) => {
          job.onProgress(() => {
            i++;
          });

          return job.waitForCompletion();
        }).andThen((job) => {
          expect(i).toBe(job.lufiFile.totalChunks);

          return okAsync(undefined);
        })
        .orElse((error) => fail(error.message));
    });
  });

  describe("Pause/Resume mecanism", () => {
    it("Should pause then resume a download", async () => {
      let lufiFile: LufiFile;
      let downloadPath: string;

      await Lufi.download(localUpload.downloadUrl())
        .andThen((job) => Lufi.pause(job))
        .andThen((job) => {
          expect(job.status).toBe(JobStatus.PAUSED);

          return okAsync(job);
        })
        .andThen((job) => Lufi.resume(job))
        .andThen((job) => {
          expect(job.status).toBe(JobStatus.ONGOING);

          return okAsync(job);
        })
        .andThen((job) => job.waitForCompletion())
        .andThen((job) => {
          if (job.downloadedFile) {
            lufiFile = job.lufiFile;
            downloadPath = downloadDir + job.lufiFile.name;

            return ResultAsync.fromPromise(
              job.downloadedFile.arrayBuffer(),
              (error) => ensureError(error),
            ).andThen((arrayBuffer) =>
              ResultAsync.fromPromise(
                Deno.writeFile(downloadPath, new Uint8Array(arrayBuffer)),
                (error) => ensureError(error),
              )
            );
          } else {
            fail("Temporary file is missing");
          }
        })
        .andThen(() => {
          const downloadedFile = openAsFile(downloadPath);

          expect(downloadedFile.size).toBe(lufiFile.size);
          expect(downloadedFile.type).toBe(lufiFile.type);
          expect(downloadedFile.name?.split("/").pop()).toBe(lufiFile.name);

          return okAsync(undefined);
        })
        .orElse((error) => fail(error.message));
    });
  });
});
