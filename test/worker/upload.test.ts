import { describe, it } from "@std/testing/bdd";
import { expect } from "@std/expect";
import mime from "mime";
import { fail } from "@std/assert";
import * as Lufi from "~/api/lufi.ts";
import { okAsync, ResultAsync } from "neverthrow";
import ProgressBar from "jsr:@deno-library/progress";
import { JobStatus } from "~/enum/job-status.ts";

describe("Upload", () => {
  const serverUrl = new URL("http://127.0.0.1:8081/");

  const file0kbPath = import.meta.dirname + "/../file-0kb";
  const file0kb = new File(
    [new Blob([Deno.readFileSync(file0kbPath)])],
    file0kbPath,
  );

  const file117kbPath = import.meta.dirname + "/../file-117kb.jpg";
  const file117kb = new File(
    [new Blob([Deno.readFileSync(file117kbPath)])],
    file117kbPath,
    {
      type: mime.getType(file117kbPath) as string,
    },
  );

  const file1mbPath = import.meta.dirname + "/../file-1mb.jpg";
  const file1mb = new File(
    [new Blob([Deno.readFileSync(file1mbPath)])],
    file1mbPath,
    {
      type: mime.getType(file1mbPath) as string,
    },
  );

  const file10mbPath = import.meta.dirname + "/../file-10mb.mp4";
  const file10mb = new File(
    [new Blob([Deno.readFileSync(file10mbPath)])],
    file10mbPath,
    {
      type: mime.getType(file10mbPath) as string,
    },
  );

  const file1GBPath = import.meta.dirname + "/../file-1gb.txt";
  const file1GB = new File(
    [new Blob([Deno.readFileSync(file1GBPath)])],
    file1GBPath,
    {
      type: mime.getType(file1GBPath) as string,
    },
  );

  const password = "mdp";
  const hashedPassword =
    "2118c37356b669d52c22510c0287642551fcdc1b9b27517999e040ad56d1ad678cb71496eb4da19832143ae14ef1ceabf1824349bd608176a91f22f7088a5427";

  describe("Without password", () => {
    it("Should upload a 0kb file", async () => {
      const jobResult = await Lufi.upload(serverUrl, [file0kb]);

      if (jobResult.isOk()) {
        const uploadJob = jobResult.value[0];

        await uploadJob.waitForCompletion().andThen(({ lufiFile }) => {
          expect(lufiFile.keys.server).not.toBe("");
          expect(lufiFile.chunksReady).toBe(lufiFile.totalChunks);
          expect(lufiFile.password).toBe("");

          return okAsync(lufiFile);
        })
          .andThen((lufiFile) => Lufi.infos(lufiFile.downloadUrl()))
          .map((job) => {
            expect(job.lufiFile.delay).toBe(uploadJob.lufiFile.delay);
            expect(job.lufiFile.delAtFirstView).toBe(
              uploadJob.lufiFile.delAtFirstView,
            );
            expect(job.lufiFile.size).toBe(uploadJob.lufiFile.size);
            expect(job.lufiFile.type).toBe(uploadJob.lufiFile.type);
            expect(job.lufiFile.zipped).toBe(uploadJob.lufiFile.zipped);
            expect(job.lufiFile.name).toBe(uploadJob.lufiFile.name);
          });
      } else {
        fail(jobResult.error.message);
      }
    });

    it("Should upload a 117kb file", async () => {
      const jobResult = await Lufi.upload(serverUrl, [file117kb]);

      if (jobResult.isOk()) {
        const uploadJob = jobResult.value[0];

        await uploadJob.waitForCompletion().andThen(({ lufiFile }) => {
          expect(lufiFile.keys.server).not.toBe("");
          expect(lufiFile.chunksReady).toBe(lufiFile.totalChunks);
          expect(lufiFile.password).toBe("");

          return okAsync(lufiFile);
        })
          .andThen((lufiFile) => Lufi.infos(lufiFile.downloadUrl()))
          .map((job) => {
            expect(job.lufiFile.delay).toBe(uploadJob.lufiFile.delay);
            expect(job.lufiFile.delAtFirstView).toBe(
              uploadJob.lufiFile.delAtFirstView,
            );
            expect(job.lufiFile.size).toBe(uploadJob.lufiFile.size);
            expect(job.lufiFile.type).toBe(uploadJob.lufiFile.type);
            expect(job.lufiFile.zipped).toBe(uploadJob.lufiFile.zipped);
            expect(job.lufiFile.name).toBe(uploadJob.lufiFile.name);
          });
      } else {
        fail(jobResult.error.message);
      }
    });

    it("Should upload a 1mb file", async () => {
      const jobResult = await Lufi.upload(serverUrl, [file1mb]);

      if (jobResult.isOk()) {
        const uploadJob = jobResult.value[0];

        await uploadJob
          .waitForCompletion()
          .andThen(({ lufiFile }) => {
            expect(lufiFile.keys.server).not.toBe("");
            expect(lufiFile.chunksReady).toBe(lufiFile.totalChunks);
            expect(lufiFile.password).toBe("");

            return okAsync(lufiFile);
          })
          .andThen((lufiFile) => Lufi.infos(lufiFile.downloadUrl()))
          .andThen((job) => {
            expect(job.lufiFile.delay).toBe(uploadJob.lufiFile.delay);
            expect(job.lufiFile.delAtFirstView).toBe(
              uploadJob.lufiFile.delAtFirstView,
            );
            expect(job.lufiFile.size).toBe(uploadJob.lufiFile.size);
            expect(job.lufiFile.type).toBe(uploadJob.lufiFile.type);
            expect(job.lufiFile.zipped).toBe(uploadJob.lufiFile.zipped);
            expect(job.lufiFile.name).toBe(uploadJob.lufiFile.name);

            return okAsync(undefined);
          });
      } else {
        fail(jobResult.error.message);
      }
    });

    it("Should upload a 10mb file", async () => {
      const jobResult = await Lufi.upload(serverUrl, [file10mb]);

      if (jobResult.isOk()) {
        const uploadJob = jobResult.value[0];

        await uploadJob
          .waitForCompletion()
          .andThen(({ lufiFile }) => {
            expect(lufiFile.keys.server).not.toBe("");
            expect(lufiFile.chunksReady).toBe(lufiFile.totalChunks);
            expect(lufiFile.password).toBe("");

            return okAsync(lufiFile);
          })
          .andThen((lufiFile) => Lufi.infos(lufiFile.downloadUrl()))
          .andThen((job) => {
            expect(job.lufiFile.delay).toBe(uploadJob.lufiFile.delay);
            expect(job.lufiFile.delAtFirstView).toBe(
              uploadJob.lufiFile.delAtFirstView,
            );
            expect(job.lufiFile.size).toBe(uploadJob.lufiFile.size);
            expect(job.lufiFile.type).toBe(uploadJob.lufiFile.type);
            expect(job.lufiFile.zipped).toBe(uploadJob.lufiFile.zipped);
            expect(job.lufiFile.name).toBe(uploadJob.lufiFile.name);

            return okAsync(undefined);
          });
      } else {
        fail(jobResult.error.message);
      }
    });

    it("Should upload a 1GB file", async () => {
      const jobResult = await Lufi.upload(serverUrl, [file1GB]);

      if (jobResult.isOk()) {
        const uploadJob = jobResult.value[0];

        const progress = new ProgressBar({
          title: `Uploading ${uploadJob.lufiFile.name}`,
          total: uploadJob.lufiFile.totalChunks,
        });

        await uploadJob
          .onProgress(async () => {
            await progress.render(uploadJob.lufiFile.chunksReady);
          })
          .waitForCompletion()
          .andThen(({ lufiFile }) => {
            expect(lufiFile.keys.server).not.toBe("");
            expect(lufiFile.chunksReady).toBe(lufiFile.totalChunks);
            expect(lufiFile.password).toBe("");

            return okAsync(lufiFile);
          })
          .andThen((lufiFile) => Lufi.infos(lufiFile.downloadUrl()))
          .map((job) => {
            expect(job.lufiFile.delay).toBe(uploadJob.lufiFile.delay);
            expect(job.lufiFile.delAtFirstView).toBe(
              uploadJob.lufiFile.delAtFirstView,
            );
            expect(job.lufiFile.size).toBe(uploadJob.lufiFile.size);
            expect(job.lufiFile.type).toBe(uploadJob.lufiFile.type);
            expect(job.lufiFile.zipped).toBe(uploadJob.lufiFile.zipped);
            expect(job.lufiFile.name).toBe(uploadJob.lufiFile.name);
          })
          .orElse((error) => fail(error.message));
      } else {
        fail(jobResult.error.message);
      }
    });

    it("Should upload multiple files", async () => {
      await Lufi.upload(serverUrl, [file117kb, file1mb, file10mb])
        .andThen((jobs) =>
          ResultAsync.combine(
            jobs.map((uploadJob) =>
              uploadJob.waitForCompletion().andThen(({ lufiFile }) => {
                expect(lufiFile.keys.server).not.toBe("");
                expect(lufiFile.chunksReady).toBe(lufiFile.totalChunks);

                return okAsync(lufiFile);
              })
                .andThen((lufiFile) => Lufi.infos(lufiFile.downloadUrl()))
                .map((job) => {
                  expect(job.lufiFile.delay).toBe(uploadJob.lufiFile.delay);
                  expect(job.lufiFile.delAtFirstView).toBe(
                    uploadJob.lufiFile.delAtFirstView,
                  );
                  expect(job.lufiFile.size).toBe(uploadJob.lufiFile.size);
                  expect(job.lufiFile.type).toBe(uploadJob.lufiFile.type);
                  expect(job.lufiFile.zipped).toBe(uploadJob.lufiFile.zipped);
                  expect(job.lufiFile.name).toBe(uploadJob.lufiFile.name);
                })
            ),
          )
        )
        .orElse((error) => fail(error.message));
    });
  });

  describe("With password", () => {
    it("Should upload a 117kb file", async () => {
      const jobResult = await Lufi.upload(
        serverUrl,
        [file117kb],
        undefined,
        undefined,
        undefined,
        undefined,
        password,
      );

      if (jobResult.isOk()) {
        const uploadJob = jobResult.value[0];

        await uploadJob.waitForCompletion().andThen(({ lufiFile }) => {
          expect(lufiFile.keys.server).not.toBe("");
          expect(lufiFile.chunksReady).toBe(lufiFile.totalChunks);
          expect(lufiFile.password).toBe(hashedPassword);

          return okAsync(lufiFile);
        })
          .andThen((lufiFile) => Lufi.infos(lufiFile.downloadUrl(), password))
          .map((job) => {
            expect(job.lufiFile.delay).toBe(uploadJob.lufiFile.delay);
            expect(job.lufiFile.delAtFirstView).toBe(
              uploadJob.lufiFile.delAtFirstView,
            );
            expect(job.lufiFile.size).toBe(uploadJob.lufiFile.size);
            expect(job.lufiFile.type).toBe(uploadJob.lufiFile.type);
            expect(job.lufiFile.zipped).toBe(uploadJob.lufiFile.zipped);
            expect(job.lufiFile.name).toBe(uploadJob.lufiFile.name);
          });
      } else {
        fail(jobResult.error.message);
      }
    });

    it("Should upload a 1mb file", async () => {
      const jobResult = await Lufi.upload(
        serverUrl,
        [file1mb],
        undefined,
        undefined,
        undefined,
        undefined,
        password,
      );

      if (jobResult.isOk()) {
        const uploadJob = jobResult.value[0];

        await uploadJob.waitForCompletion().andThen(({ lufiFile }) => {
          expect(lufiFile.keys.server).not.toBe("");
          expect(lufiFile.chunksReady).toBe(lufiFile.totalChunks);
          expect(lufiFile.password).toBe(hashedPassword);

          return okAsync(lufiFile);
        })
          .andThen((lufiFile) => Lufi.infos(lufiFile.downloadUrl(), password))
          .map((job) => {
            expect(job.lufiFile.delay).toBe(uploadJob.lufiFile.delay);
            expect(job.lufiFile.delAtFirstView).toBe(
              uploadJob.lufiFile.delAtFirstView,
            );
            expect(job.lufiFile.size).toBe(uploadJob.lufiFile.size);
            expect(job.lufiFile.type).toBe(uploadJob.lufiFile.type);
            expect(job.lufiFile.zipped).toBe(uploadJob.lufiFile.zipped);
            expect(job.lufiFile.name).toBe(uploadJob.lufiFile.name);
          });
      } else {
        fail(jobResult.error.message);
      }
    });

    it("Should upload a 10mb file", async () => {
      const jobResult = await Lufi.upload(
        serverUrl,
        [file10mb],
        undefined,
        undefined,
        undefined,
        undefined,
        password,
      );

      if (jobResult.isOk()) {
        const uploadJob = jobResult.value[0];

        await uploadJob.waitForCompletion().andThen(({ lufiFile }) => {
          expect(lufiFile.keys.server).not.toBe("");
          expect(lufiFile.chunksReady).toBe(lufiFile.totalChunks);
          expect(lufiFile.password).toBe(hashedPassword);

          return okAsync(lufiFile);
        })
          .andThen((lufiFile) => Lufi.infos(lufiFile.downloadUrl(), password))
          .map((job) => {
            expect(job.lufiFile.delay).toBe(uploadJob.lufiFile.delay);
            expect(job.lufiFile.delAtFirstView).toBe(
              uploadJob.lufiFile.delAtFirstView,
            );
            expect(job.lufiFile.size).toBe(uploadJob.lufiFile.size);
            expect(job.lufiFile.type).toBe(uploadJob.lufiFile.type);
            expect(job.lufiFile.zipped).toBe(uploadJob.lufiFile.zipped);
            expect(job.lufiFile.name).toBe(uploadJob.lufiFile.name);
          });
      } else {
        fail(jobResult.error.message);
      }
    });

    it("Should upload multiple files", async () => {
      await Lufi.upload(
        serverUrl,
        [file117kb, file1mb, file10mb],
        undefined,
        undefined,
        undefined,
        undefined,
        password,
      )
        .andThen((jobs) =>
          ResultAsync.combine(
            jobs.map((uploadJob) =>
              uploadJob.waitForCompletion().andThen(({ lufiFile }) => {
                expect(lufiFile.keys.server).not.toBe("");
                expect(lufiFile.chunksReady).toBe(lufiFile.totalChunks);
                expect(lufiFile.password).toBe(hashedPassword);

                return okAsync(lufiFile);
              })
                .andThen((lufiFile) =>
                  Lufi.infos(lufiFile.downloadUrl(), password)
                )
                .map((job) => {
                  expect(job.lufiFile.delay).toBe(uploadJob.lufiFile.delay);
                  expect(job.lufiFile.delAtFirstView).toBe(
                    uploadJob.lufiFile.delAtFirstView,
                  );
                  expect(job.lufiFile.size).toBe(uploadJob.lufiFile.size);
                  expect(job.lufiFile.type).toBe(uploadJob.lufiFile.type);
                  expect(job.lufiFile.zipped).toBe(uploadJob.lufiFile.zipped);
                  expect(job.lufiFile.name).toBe(uploadJob.lufiFile.name);
                })
            ),
          )
        )
        .orElse((error) => fail(error.message));
    });
  });

  describe("With 1 day delay", () => {
    it("Should upload a 117kb file", async () => {
      const jobResult = await Lufi.upload(serverUrl, [file117kb], 1);

      if (jobResult.isOk()) {
        const uploadJob = jobResult.value[0];

        await uploadJob.waitForCompletion().andThen(({ lufiFile }) => {
          expect(lufiFile.keys.server).not.toBe("");
          expect(lufiFile.chunksReady).toBe(lufiFile.totalChunks);
          expect(lufiFile.delay).toBe(1);

          return okAsync(lufiFile);
        });
      } else {
        fail(jobResult.error.message);
      }
    });

    it("Should upload a 1mb file", async () => {
      const jobResult = await Lufi.upload(serverUrl, [file1mb], 1);

      if (jobResult.isOk()) {
        const uploadJob = jobResult.value[0];

        await uploadJob.waitForCompletion().andThen(({ lufiFile }) => {
          expect(lufiFile.keys.server).not.toBe("");
          expect(lufiFile.chunksReady).toBe(lufiFile.totalChunks);
          expect(lufiFile.delay).toBe(1);

          return okAsync(lufiFile);
        })
          .andThen((lufiFile) => Lufi.infos(lufiFile.downloadUrl()))
          .map((job) => {
            expect(job.lufiFile.delay).toBe(uploadJob.lufiFile.delay);
            expect(job.lufiFile.delAtFirstView).toBe(
              uploadJob.lufiFile.delAtFirstView,
            );
            expect(job.lufiFile.size).toBe(uploadJob.lufiFile.size);
            expect(job.lufiFile.type).toBe(uploadJob.lufiFile.type);
            expect(job.lufiFile.zipped).toBe(uploadJob.lufiFile.zipped);
            expect(job.lufiFile.name).toBe(uploadJob.lufiFile.name);
          });
      } else {
        fail(jobResult.error.message);
      }
    });

    it("Should upload a 10mb file", async () => {
      const jobResult = await Lufi.upload(serverUrl, [file10mb], 1);

      if (jobResult.isOk()) {
        const uploadJob = jobResult.value[0];

        await uploadJob.waitForCompletion().andThen(({ lufiFile }) => {
          expect(lufiFile.keys.server).not.toBe("");
          expect(lufiFile.chunksReady).toBe(lufiFile.totalChunks);
          expect(lufiFile.delay).toBe(1);

          return okAsync(lufiFile);
        })
          .andThen((lufiFile) => Lufi.infos(lufiFile.downloadUrl()))
          .map((job) => {
            expect(job.lufiFile.delay).toBe(uploadJob.lufiFile.delay);
            expect(job.lufiFile.delAtFirstView).toBe(
              uploadJob.lufiFile.delAtFirstView,
            );
            expect(job.lufiFile.size).toBe(uploadJob.lufiFile.size);
            expect(job.lufiFile.type).toBe(uploadJob.lufiFile.type);
            expect(job.lufiFile.zipped).toBe(uploadJob.lufiFile.zipped);
            expect(job.lufiFile.name).toBe(uploadJob.lufiFile.name);
          });
      } else {
        fail(jobResult.error.message);
      }
    });

    it("Should upload multiple files", async () => {
      await Lufi.upload(
        serverUrl,
        [file117kb, file1mb, file10mb],
        1,
      )
        .andThen((jobs) =>
          ResultAsync.combine(
            jobs.map((uploadJob) =>
              uploadJob.waitForCompletion().andThen(({ lufiFile }) => {
                expect(lufiFile.keys.server).not.toBe("");
                expect(lufiFile.chunksReady).toBe(lufiFile.totalChunks);
                expect(lufiFile.delay).toBe(1);

                return okAsync(lufiFile);
              })
                .andThen((lufiFile) => Lufi.infos(lufiFile.downloadUrl()))
                .map((job) => {
                  expect(job.lufiFile.delay).toBe(uploadJob.lufiFile.delay);
                  expect(job.lufiFile.delAtFirstView).toBe(
                    uploadJob.lufiFile.delAtFirstView,
                  );
                  expect(job.lufiFile.size).toBe(uploadJob.lufiFile.size);
                  expect(job.lufiFile.type).toBe(uploadJob.lufiFile.type);
                  expect(job.lufiFile.zipped).toBe(uploadJob.lufiFile.zipped);
                  expect(job.lufiFile.name).toBe(uploadJob.lufiFile.name);
                })
            ),
          )
        )
        .orElse((error) => fail(error.message));
    });
  });

  describe("Pause/resume mecanism", () => {
    it("Should resume an upload after pausing it", async () => {
      const uploadJobResult = await Lufi.upload(serverUrl, [file10mb]);

      if (uploadJobResult.isOk()) {
        const uploadJob = uploadJobResult.value[0];

        await Lufi.pause(uploadJob)
          .andThen((job) => {
            expect(job.status).toBe(JobStatus.PAUSED);

            return okAsync(job);
          })
          .andThen((job) => Lufi.resume(job))
          .andThen((job) => {
            expect(job.status).toBe(JobStatus.ONGOING);

            return okAsync(job);
          })
          .andThen((job) => job.waitForCompletion())
          .andThen((job) => {
            expect(job.lufiFile.keys.server).not.toBe("");
            expect(job.lufiFile.chunksReady).toBe(job.lufiFile.totalChunks);

            return okAsync(job.lufiFile);
          })
          .andThen((lufiFile) => Lufi.infos(lufiFile.downloadUrl()))
          .map((job) => {
            expect(job.lufiFile.delay).toBe(uploadJob.lufiFile.delay);
            expect(job.lufiFile.delAtFirstView).toBe(
              uploadJob.lufiFile.delAtFirstView,
            );
            expect(job.lufiFile.size).toBe(uploadJob.lufiFile.size);
            expect(job.lufiFile.type).toBe(uploadJob.lufiFile.type);
            expect(job.lufiFile.zipped).toBe(uploadJob.lufiFile.zipped);
            expect(job.lufiFile.name).toBe(uploadJob.lufiFile.name);
          })
          .orElse((error) => fail(error.message));
      } else {
        fail(uploadJobResult.error.message);
      }
    });
  });

  // Do not test it until Deno fixes https://github.com/denoland/deno/issues/26739
  describe.skip("As a zip", () => {
    it("Should upload 1 file as a Zip using default name", async () => {
      await Lufi.upload(serverUrl, [file117kb], undefined, undefined, true)
        .andThen((jobs) => jobs[0].waitForCompletion()).andThen(
          ({ lufiFile }) => {
            expect(lufiFile.chunksReady).toBe(lufiFile.totalChunks);
            expect(lufiFile.keys.server).not.toBe("");
            expect(lufiFile.name).toBe("documents.zip"); // Default filename
            expect(lufiFile.type).toBe("application/zip");

            return okAsync(undefined);
          },
        ).orElse((error) => fail(error.message));
    });

    it("Should upload 1 file as a Zip using other name", async () => {
      const name = "name.zip";
      await Lufi.upload(
        serverUrl,
        [file117kb],
        undefined,
        undefined,
        true,
        name,
      )
        .andThen((jobs) => jobs[0].waitForCompletion()).andThen(
          ({ lufiFile }) => {
            expect(lufiFile.chunksReady).toBe(lufiFile.totalChunks);
            expect(lufiFile.keys.server).not.toBe("");
            expect(lufiFile.name).toBe(name); // Default filename
            expect(lufiFile.type).toBe("application/zip");

            return okAsync(undefined);
          },
        ).orElse((error) => fail(error.message));
    });

    it("Should upload multiple files as a Zip using default name", async () => {
      await Lufi.upload(
        serverUrl,
        [file117kb, file1mb, file10mb],
        undefined,
        undefined,
        true,
      )
        .andThen((jobs) => jobs[0].waitForCompletion()).andThen(
          ({ lufiFile }) => {
            expect(lufiFile.chunksReady).toBe(lufiFile.totalChunks);
            expect(lufiFile.keys.server).not.toBe("");
            expect(lufiFile.name).toBe("documents.zip"); // Default filename
            expect(lufiFile.type).toBe("application/zip");

            return okAsync(undefined);
          },
        ).orElse((error) => fail(error.message));
    });

    it("Should upload multiple files (total: 5GB) as a Zip using default name", async () => {
      await Lufi.upload(
        serverUrl,
        [file1GB],
        undefined,
        undefined,
        true,
      )
        .andThen((jobs) => {
          const progress = new ProgressBar({
            title: `Uploading ${jobs[0].lufiFile.name}`,
            total: jobs[0].lufiFile.totalChunks,
          });
          return okAsync({ job: jobs[0], progress });
        }).andThen(({ job, progress }) =>
          job.onProgress(async () => {
            await progress.render(job.lufiFile.chunksReady);
          }).waitForCompletion()
        ).andThen(
          ({ lufiFile }) => {
            expect(lufiFile.chunksReady).toBe(lufiFile.totalChunks);
            expect(lufiFile.keys.server).not.toBe("");
            expect(lufiFile.name).toBe("documents.zip"); // Default filename
            expect(lufiFile.type).toBe("application/zip");

            return okAsync(undefined);
          },
        ).orElse((error) => fail(error.message));
    });
  });
});

// const downloadLink = (lufiFile: LufiFile) =>
//   console.info(`Download link : ${lufiFile.downloadUrl()}`);
