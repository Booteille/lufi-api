import { describe, it } from "@std/testing/bdd";
import { expect } from "@std/expect";
import mime from "mime";
import * as Lufi from "~/api/lufi.ts";
import { fail } from "@std/assert";

describe("Remove", () => {
  const serverUrl = new URL("http://127.0.0.1:8081/");

  const file117kbPath = import.meta.dirname + "/../file-117kb.jpg";
  const file117kb = new File(
    [new Blob([Deno.readFileSync(file117kbPath)])],
    file117kbPath,
    {
      type: mime.getType(file117kbPath) as string,
    },
  );

  it("Should remove an uploaded file", async () => {
    await Lufi.upload(serverUrl, [file117kb])
      .andThen((uploadJobs) => uploadJobs[0].waitForCompletion())
      .orElse((error) => fail(error.message))
      .andThen((uploadJob) => Lufi.remove(uploadJob.lufiFile.removeUrl()))
      .andThen((job) => Lufi.infos(job.lufiFile.downloadUrl()))
      .map(() => {
        fail("Should not be able to retrieve file infos");
      })
      .mapErr((error) => {
        expect(error).toBeInstanceOf(Error);
      });
  });
});
