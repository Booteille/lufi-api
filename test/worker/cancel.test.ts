import { describe, it } from "@std/testing/bdd";
import { expect } from "@std/expect";
import mime from "mime";
import * as Lufi from "~/api/lufi.ts";
import { UPLOAD_STATUS } from "~/enum/file-status.ts";
import { fail } from "@std/assert";

describe("Cancel", () => {
  const serverUrl = new URL("http://127.0.0.1:8081/");

  const file10mbPath = import.meta.dirname + "/../file-10mb.mp4";
  const file10mb = new File(
    [new Blob([Deno.readFileSync(file10mbPath)])],
    file10mbPath,
    {
      type: mime.getType(file10mbPath) as string,
    },
  );

  it("Should cancel a file being uploaded", async () => {
    await Lufi.upload(serverUrl, [file10mb])
      .orElse((error) => fail(error.message))
      .andThen((uploadJobs) => Lufi.cancel(uploadJobs[0]))
      .map((cancelJob) => {
        expect(cancelJob.lufiFile.uploadStatus).toBe(UPLOAD_STATUS.CANCELED);
      })
      .mapErr((error) => fail(error.message));
  });
});
