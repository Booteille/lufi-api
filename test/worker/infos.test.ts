import mime from "mime";
import * as Lufi from "~/api/lufi.ts";
import { okAsync } from "neverthrow";
import { describe, it } from "@std/testing/bdd";
import { expect } from "@std/expect";
import { fail } from "@std/assert/fail";

describe("Informations", () => {
  const serverUrl = new URL("http://127.0.0.1:8081/");

  const file117kbPath = import.meta.dirname + "/../file-117kb.jpg";
  const file117kb = new File(
    [new Blob([Deno.readFileSync(file117kbPath)])],
    file117kbPath,
    {
      type: mime.getType(file117kbPath) as string,
    },
  );

  const externalUrl = new URL(
    "https://asso.framasoft.org/drop/r/uDvKt1bLTp#t7As+ojgCxSrEYrlu9+PSAL3j+GH95VZ2aX1hPIWypY=",
  );
  const externalUrlWithPassword = new URL(
    "https://asso.framasoft.org/drop/r/Xv01n9yWML#MrAAe/ARA9+R+6xEQ32G6/Vzw0Gazk06whJegR+oDyU=",
  );

  const password = "mdp";
  const delay = 1;

  const externalDelay = "1825";
  const externalSize = 10486584;
  const externalType = "video/mp4";
  const externalName = "file-10mb.mp4";

  describe("With password", () => {
    it("Should retrieve informations of an uploaded file", async () => {
      await Lufi.upload(
        serverUrl,
        [file117kb],
        delay,
        undefined,
        undefined,
        undefined,
        password,
      )
        .andThen((uploadJobs) => uploadJobs[0].waitForCompletion())
        .andThen((uploadJob) =>
          Lufi.infos(uploadJob.lufiFile.downloadUrl(), password)
        )
        .andThen((infoJob) => {
          expect(infoJob.lufiFile.delay).toBe(delay);
          expect(infoJob.lufiFile.delAtFirstView).toBeFalsy();
          expect(infoJob.lufiFile.size).toBe(file117kb.size);
          expect(infoJob.lufiFile.type).toBe(file117kb.type);
          expect(infoJob.lufiFile.zipped).toBeFalsy();
          expect(infoJob.lufiFile.name).toBe(
            file117kb.name.split("/").pop() as string,
          );

          return okAsync(undefined);
        })
        .orElse((error) => fail(error.message));
    });

    it("Should retrieve informations of an external file", async () => {
      await Lufi.infos(externalUrlWithPassword, password)
        .andThen((infoJob) => {
          expect(infoJob.lufiFile.delay).toBe(externalDelay);
          expect(infoJob.lufiFile.delAtFirstView).toBeFalsy();
          expect(infoJob.lufiFile.size).toBe(externalSize);
          expect(infoJob.lufiFile.type).toBe(externalType);
          expect(infoJob.lufiFile.zipped).toBeFalsy();
          expect(infoJob.lufiFile.name).toBe(externalName);

          return okAsync(undefined);
        })
        .orElse((error) => fail(error.message));
    });
  });

  describe("Without password", () => {
    it("Should retrieve informations of an uploaded file", async () => {
      await Lufi.upload(
        serverUrl,
        [file117kb],
        delay,
      )
        .andThen((uploadJobs) => uploadJobs[0].waitForCompletion())
        .andThen((uploadJob) => Lufi.infos(uploadJob.lufiFile.downloadUrl()))
        .andThen((infoJob) => {
          expect(infoJob.lufiFile.delay).toBe(delay);
          expect(infoJob.lufiFile.delAtFirstView).toBeFalsy();
          expect(infoJob.lufiFile.size).toBe(file117kb.size);
          expect(infoJob.lufiFile.type).toBe(file117kb.type);
          expect(infoJob.lufiFile.zipped).toBeFalsy();
          expect(infoJob.lufiFile.name).toBe(
            file117kb.name.split("/").pop() as string,
          );

          return okAsync(undefined);
        })
        .orElse((error) => fail(error.message));
    });

    it("Should retrieve informations of an external file", async () => {
      await Lufi.infos(externalUrl)
        .andThen((infoJob) => {
          expect(infoJob.lufiFile.delay).toBe(externalDelay);
          expect(infoJob.lufiFile.delAtFirstView).toBeFalsy();
          expect(infoJob.lufiFile.size).toBe(externalSize);
          expect(infoJob.lufiFile.type).toBe(externalType);
          expect(infoJob.lufiFile.zipped).toBeFalsy();
          expect(infoJob.lufiFile.name).toBe(externalName);

          return okAsync(undefined);
        })
        .orElse((error) => fail(error.message));
    });
  });
});
