import * as esbuild from "esbuild";
import { denoPlugins } from "jsr:@luca/esbuild-deno-loader";
import workerPlugin from "npm:@chialab/esbuild-plugin-worker";

await esbuild.build({
  plugins: [...denoPlugins(), workerPlugin()],
  entryPoints: ["./src/index.ts", "./src/worker/*.ts"],
  outdir: "./dist/",
  bundle: true,
  format: "esm",
  minify: true,
  sourcemap: false,
  target: ["deno2", "chrome67", "firefox68"],
});

esbuild.stop();
